from tensorflow.python.keras.models import Model
from tensorflow.python.keras.layers import Input, Dense, Dropout, Flatten
from tensorflow.python.keras import regularizers
from tensorflow.python.keras.callbacks import EarlyStopping
from tensorflow.python.keras.layers import Conv1D, MaxPooling1D, UpSampling1D

def build_autoencoder(
    input_dim, layers_dim=[100, 10, 10],
    activations=['tanh', 'sigmoid'],
    inits=['glorot_normal', 'glorot_normal'],
    optimizer='adam',
    l2=0,
    loss='mse',
    *args, **kwargs):

    input_row = Input(shape=input_dim[0])

    for n, layer_dim in enumerate(layers_dim):
        if n == 0:
            encoded = Dense(layer_dim, activation=activations[0],
                            kernel_initializer=inits[0])(input_row)
        elif n < (len(layers_dim) - 1):
            encoded = Dense(layer_dim, activation=activations[0],
                            kernel_initializer=inits[0])(encoded)
        else:
            encoded = Dense(layer_dim, activation=activations[0],
                            # activity_regularizer=regularizers.l2(l2),
                            kernel_initializer=inits[0])(encoded)

    encoder = Model(input_row, encoded)
    # create a placeholder for an encoded (n-dimensional) input
    encoded_input = Input(shape=(layers_dim[-1],))

    for n, layer_dim in enumerate(reversed(layers_dim[:-1])):
        if n == 0:
            decoded = Dense(layer_dim, activation=activations[0],
                            kernel_initializer=inits[0])(encoded)
        else:
            decoded = Dense(layer_dim, activation=activations[0],
                            kernel_regularizer=regularizers.l2(l2),
                            kernel_initializer=inits[0])(decoded)

    decoded = Dense(input_dim[0], activation=activations[1],
                    kernel_regularizer=regularizers.l2(l2),
                    kernel_initializer=inits[1])(decoded)

    autoencoder = Model(input_row, decoded)

    encoded_input = Input(shape=(layers_dim[-1],))
    for n, layer_dim in enumerate(reversed(layers_dim)):
        if n == 0:
            deco = autoencoder.layers[-len(layers_dim)](encoded_input)
        else:
            deco = autoencoder.layers[-(len(layers_dim) - n)](deco)
    # create the decoder model
    decoder = Model(encoded_input, deco)

    autoencoder.compile(optimizer=optimizer, loss=loss)

    return autoencoder, encoder, decoder

def build_1dcnn_encoder(input_node, conv_args):
    x = Conv1D(16, 3, **conv_args)(input_node)
    x = MaxPooling1D(2, padding='same')(x)
    x = Conv1D(8, 3, **conv_args)(x)
    x = MaxPooling1D(2, padding='same')(x)
    x = Conv1D(8, 3, **conv_args)(x)
    encoded = MaxPooling1D(2, padding='same', name='encoded')(x)
    return encoded

def build_1dcnn_ae(
    input_dim, 
    optimizer='adam',
    loss='mse',
    *args, **kwargs):

    conv_args = dict(activation='relu', padding='same')

    input_series = Input(shape=input_dim)
    #input_img = Input(shape=(28, 28, 1))  # adapt this if using `channels_first` image data format
    encoded = build_1dcnn_encoder(input_series, conv_args)

    x = Conv1D(16, 3, **conv_args)(input_series)
    # x = MaxPooling1D(2, padding='same')(x)
    #x = Conv1D(8, 3, **conv_args)(x)
    #x = MaxPooling1D(2, padding='same')(x)
    #x = Conv1D(8, 3, **conv_args)(x)
    encoded = MaxPooling1D(2, padding='same', name='encoded')(x)

    x = Conv1D(8, 3, **conv_args)(encoded)
    x = UpSampling1D(2)(x)
    #x = Conv1D(8, 3, **conv_args)(x)
    #x = UpSampling1D(2)(x)
    #x = Conv1D(16, 3, activation='relu')(x)
    #x = UpSampling1D(2)(x)
    decoded = Conv1D(input_dim[-1], 3, **conv_args)(x)

    autoencoder = Model(input_series, decoded)
    autoencoder.compile(optimizer=optimizer, loss=loss)
    # Build encoder and decoder
    encoder = Model(input_series, encoded)
    # encoded_input = Input(shape=autoencoder.get_layer('encoded').get_output_shape_at(0)[1:])
    # retrieve the last layer of the autoencoder model
    #decoder_layer = autoencoder.layers[-1]
    # create the decoder model
    #decoder = Model(encoded_input, decoder_layer)#decoder_layer(encoded_input))
    return autoencoder, encoder, None # TODO: implement decoder


class BaseEncoder():
    '''
    Base class for building Encoders

    Parameters
    ----------
    build_fun: Callable
        build model function that returns a Keras model
    build_params: dict
        Extra parameters for build_fun
    batch_size: int
        Number of samples per batch. Ignored when using `fit_generator`
    epochs: int
        Maximum number of epochs
    patience: int
        If validation data is passed to the `fit`, `fit_batches`, or `fit_generator` 
        then stops training after the loss metric in the validation set has not increased 
        for `patience` epochs
    scaler
        Scikit learn preprocessing object that normalizes input
    val_split: float
        Value between ``[0,1]`` that indicates the percentage of data to be used for validation
    verbose: bool
        Prints a variety of information including model summary    
    '''
    def __init__(self, 
        build_fun   = None,
        build_params={
            'optimizer': 'adam', 
            'loss': 'mse'},
        epochs      =1000, 
        batch_size  =100,
        patience    =10, 
        scaler      =None,
        val_split   =0.1,
        verbose     =False,
        *args, **kwargs):

        self.build_fun      = build_fun
        self.build_params   = build_params
        self.batch_size     = batch_size
        self.epochs         = epochs
        self.verbose        = verbose
        self.sc             = scaler
        self.patience       = patience
        self.val_split      = val_split

    def fit(self, X, y=None):
        self.ae, self.encoder, self.decoder = self.build_fun(
            X.shape[1:], **self.build_params)

        if self.verbose:
            self.ae.summary()

        if self.sc:
            self.Xs = self.sc.fit_transform(X)
        else:
            self.Xs = X

        self.ae.fit(self.Xs, self.Xs, batch_size=self.batch_size,
                    epochs=self.epochs, shuffle=False,
                    verbose=self.verbose, validation_split=self.val_split,
                    callbacks=[
                        EarlyStopping(monitor='val_loss',
                                      min_delta=0, patience=self.patience,
                                      verbose=1,
                                      mode='auto'), ])
        
        return self

    def transform(self, X, y=None):
        if self.sc:
            Xs = self.sc.transform(X)
        else:
            Xs = X

        if hasattr(self, 'encoder'):
            Xe = self.encoder.predict(Xs, verbose=self.verbose,
                                      batch_size=self.batch_size)
        else:
            print('Error: fit first')
        return Xe

    def inverse_transform(self, X):

        Xp = self.decoder.predict(X, verbose=self.verbose,
                                  batch_size=self.batch_size)
        if self.sc:
            Xr = self.sc.inverse_transform(Xp)
        else:
            Xr = Xp

        #raise NotImplementedError
        return Xr

    def fit_transform(self, X, y=None, **fit_params):
        return self.fit(X, **fit_params).transform(X)


class AutoEncoder(BaseEncoder):
    '''
    Tabular data Auto-Encoder
    '''

    def __init__(self, build_fun=build_autoencoder,
                layers_dim=[100,10,10],
                 *args, **kwargs):

        super().__init__(build_fun=build_fun, *args, **kwargs)
        self.build_params['layers_dim'] = layers_dim
        #self.build_fun=lambda x: build_fun(x, **self.build_params)

class SeriesAE(BaseEncoder):
    '''
    Time Series Auto-Encoder
    '''
    def __init__(self, build_fun=build_1dcnn_ae,
                 *args, **kwargs):
        super().__init__(build_fun=build_fun, *args, **kwargs)
 