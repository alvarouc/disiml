import numpy as np
from tqdm import tqdm
import os
import h5py
import pickle as pkl
import pandas as pd
from multiprocessing import Pool
# from multiprocessing.pool import ThreadPool
from functools import partial
from glob import iglob, glob
import tensorflow as tf

class Broker:
    def __init__(self, 
        data_path=None, 
        cache_size=8, # In GBs
        preprocess_fun=None,
        cache_folder=None, 
        aux=None):
        '''
        Input:
        - data_path: path to file
        - buffer_size: Buffer size in RAM GB
        - preprocess_fun: function to apply to read data from disk
        - cache_folder: Save buffer in numpy format for faster loading
        '''
        self.data_path = data_path
        self.cache_size = cache_size
        self.preprocess_fun = preprocess_fun
        self.cache_folder = cache_folder
        self.aux = aux
        # internals
        self.index = {}  # maps the names to idxs
        self.pointer = 0
        self.cache = False

    def save(self, folder_path='./'):
        '''
        Saves cache to disk for faster initialization
        '''
        folder = self.cache_folder or folder_path
        os.makedirs(folder, exist_ok=True)
        print('Saving to %s' % folder)
        np.save(folder + 'broker.npy', self.buffer)
        with open(folder + 'broker.names', 'wb') as fid:
            pkl.dump(self.index, fid)

    def load(self, folder_path):
        '''
        Loads cache from disk
        '''
        self.buffer = np.load(folder_path + '/broker.npy')
        self.index = pd.read_pickle(folder_path + '/broker.names')
        self.pointer += len(self.index)
        self.buffer_size = self.buffer.shape[0]

    def read_disk(self, name):
        '''
        Reads a single sample from disk
        '''
        sample = self.data[name][...] # the [...] is harmless and needed for hdf5 compatibility
        if self.preprocess_fun is not None:
            sample = self.preprocess_fun(sample)
        return sample

    def push(self, name):
        '''
        Reads the name from data and saves it to buffer
        Returns True if name was saved to buffer
        '''
        # If first read, compute the size of a video to allocate buffer
        if not hasattr(self, 'buffer'):
            self.load_data(self.data_path) # load_data to be defined in child classes
            img = self.read_disk(name)
            self.data_shape = img.shape
            self.buffer_size = (self.cache_size * (2**30)) / img.nbytes
            self.buffer_size = int(np.floor(self.buffer_size))

            print('First file load. Opened name {} from {:s}.'.format(
                name, self.data_path))
            print('\timg.size {}, bytes {} ({} MiB).'.format(
                img.size, img.nbytes, img.nbytes / (2**20)))
            print('Allocating {}GiB for {} videos'.format(
                img.nbytes * self.buffer_size / (2**30),
                self.buffer_size))
            print('Min-Max:{}-{}'.format(
                img.ravel().min(), img.ravel().max()))

            self.buffer = np.zeros([self.buffer_size, ] +
                                   list(img.shape), dtype=img.dtype)

        if self.pointer < self.buffer_size:

            self.index[name] = int(self.pointer)
            self.buffer[self.pointer, ...] = self.read_disk(name)
            self.pointer += 1
            return True
        else:
            return False

    # def populate(self, names, pbar=False):
    #     self.read(names, pbar=pbar)

    def read(self, names, pbar=False):

        if type(names) is str:
            names = [names, ]
        elif hasattr(names, '__iter__'):
            names = list(names)
        else:
            names = [names, ]

        if pbar:
            names_t = tqdm(names, desc='Reading')
        else:
            names_t = names

        in_cache = [self.push(name)
                       if name not in self.index.keys()
                       else True
                       for name in names_t]

        if all(in_cache):
            numeric_idx = [self.index[name] for name in names]
            out = self.buffer[numeric_idx, ...]
        else:
            # TODO: Potential for time saving
            # pre-allocate array to names
            out = [self.buffer[None, self.index[name], ...] if saved
                      else self.read_disk(name)[None, ...]
                      for saved, name in zip(in_cache, names)]
            out = np.concatenate(out, axis=0)

        if self.aux is None:
            return out
        else:
            return [out, self.aux.loc[names]]


class Brokers():
    def __init__(self, data_paths, cache_size=4,
                 preprocess_funs=None, aux=None):
                 #TODO: consider a preprocess function with all elements
        '''
        Input:
        - data_paths: list of paths to files
        - buffer_size: Buffer size in RAM GB for each input
        - preprocess_fun: function to apply to read data from disk
        '''
        self.data_paths = data_paths

        if preprocess_funs is None:
            self.preprocess_funs = [None for _ in self.data_paths]
        elif type(preprocess_funs) is list:
            assert len(preprocess_funs) == len(self.data_paths), \
                "Provide a preprocess function per file"
            self.preprocess_funs = preprocess_funs
        else:
            self.preprocess_funs = [func for func in self.preprocess_funs]

        self.cache_size = cache_size
        self.aux = aux
        # internals
        #        self.brokers # defined by each of the child classes

    def read(self, names):
        # names should be a list of names now
        data = [broker.read(names)
                for broker in self.brokers]
        if not hasattr(self, 'data_shape'):
            self.data_shape = [broker.data_shape for broker in self.brokers]

        if self.aux is None:
            return data
        else:
            return data + [self.aux.loc[names], ]

    # def populate(self, list_names):  # included for compatibility
    #     self.read(list_names)


class NPZBroker(Broker):

    def load_data(self, data_path):
        self.data = np.load(self.data_path)


class NPZBrokers(Brokers):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.brokers = [NPZBroker(data_path, preprocess_fun=fun, cache_size=self.cache_size/len(self.data_paths))
                        for data_path, fun in zip(self.data_paths, self.preprocess_funs)]


class HDFBroker(Broker):

    def load_data(self, data_path):
        self.data = h5py.File(self.data_path, 'r')

    def __exit__(self, exception_type, exception_value, traceback):
        self.data.close()


class HDFBrokers(Brokers):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.brokers = [HDFBroker(data_path, preprocess_fun=fun, cache_size=self.cache_size/len(self.data_paths))
                        for data_path, fun in zip(self.data_paths, self.preprocess_funs)]


class FileDict():
    '''
    Dictionary for loading numpy files
    '''
    def __init__(self, folder_path):
        self.folder_path = folder_path
        self.ids = None

    def keys(self):
        if self.ids is None: 
            self.ids = glob(os.path.join(self.folder_path, '*.npy'))
        return self.ids

    def __getitem__(self, key):
        sample = np.load(os.path.join(self.folder_path, str(key) + '.npy'))
        return sample

class FileBroker(Broker):
    """
    data_path: folder path containing numpy arrays
    """
    def load_data(self, data_path):
        self.data = FileDict(data_path)

class FileGenerator(tf.keras.utils.Sequence):
    '''
    File Generator class that assumes that each data sample is named as in <name>.npy and all samples reside in a `data_path` folder. See Sequence class in https://keras.io/utils/. 

    Parameters
    ----------
        data_path:str
            Absolute path to the folder that contians numpy arrays
        batch_size: int
            Number of smaples per batch
        y_set: pd.Series or pd.DataFrame
            Pandas Series/DataFrame indexed by sample name and contains a sample per row
        aux: pd.DataFrame
            Pandas Dataframe indexed by sample name where each column is a tabular data feature        
    Attributes
    ----------
        n_batches: int
            Total number of batches as computed from the number of samples / batch size
        data_shape: tuple
            Shape of a single sample. Note: this is used by the fit_generator to build the keras model.

    '''
    def __init__(self, 
            data_path : str, 
            y_set, 
            batch_size : int, 
            aux : pd.DataFrame = None):
        self.data_path = data_path
        self.batch_size = batch_size
        #self.keys = glob(os.path.join(self.data_path, '*.npy'))
        self.y_set = y_set.copy() # Pandas series with file names in index
        self.y_set = self.y_set.sample(frac=1) # random shuffle
        self.aux = aux
        # internals
        self.n_batches = int(np.ceil(self.y_set.shape[0] / float(self.batch_size)))

        # Check that all files exist
        for sample in y_set.index:
            assert os.path.exists(os.path.join(self.data_path, f'{sample}.npy'))

        # Do not load file, just get the shape
        a_sample = self.id2sample(y_set.index[0])
        self.data_shape = list(a_sample.shape)

    def id2sample(self, id):
        sample_path = os.path.join(self.data_path, f'{id}.npy')
        sample = np.load(sample_path)
        return sample

    def __len__(self):
        'Denotes the number of batches per epoch'
        return self.n_batches

    def __getitem__(self, index):
        'Generate one batch of data'
        ids = self.y_set.index[index*self.batch_size:(index+1)*self.batch_size]

        # at first run create a buffer to allocate data
        if not hasattr(self, 'buffer'):
            self.buffer = np.zeros([self.batch_size,] + self.data_shape)
        
        for i, id in enumerate(ids):
            self.buffer[i,...] = self.id2sample(id)

        if self.aux is None:
            X = self.buffer[:len(ids)]
        else:
            X = [self.buffer[:len(ids)], self.aux.loc[ids]]

        y = self.y_set.loc[ids].values

        return X,y

class FileBrokers(Brokers):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.brokers = [FileBroker(data_path=data_path, preprocess_fun=fun, cache_size=self.cache_size/len(self.data_paths))
                        for data_path, fun in zip(self.data_paths, self.preprocess_funs)]


# def load_data_core(datapath, names):

#     with h5py.File(datapath, 'r') as f:
#         train = np.array([f[name][...] for name in names])
#     return(train)


# class MIBroker_multithreaded(Broker):

#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         self.pool = Pool(processes=len(self.data_path))
#         assert type(
#             self.data_path) is list, 'data_path argument should be a list'

#     def exit_fun(self):
#         self.pool.close()

#     def read_disk(self, names):

#         # self.pool = Pool(processes=len(self.data_path)) # this is better than opening once in init
#         videos = [fl for fl in tqdm(self.pool.imap(partial(load_data_core, names=names), self.data_path),
#                                     total=len(self.data_path), desc='Populating data')]
#         videos = self.preprocess_fun(videos)
#         # self.pool.close()

#         return videos

#     def push(self, names):
#         '''
#         Reads the name from data and saves it to buffer
#         Returns True if name was saved to buffer
#         '''
#         tmp_names = pd.Series([])
#         tmp_names = tmp_names[names]
#         tmp_names[names] = list(range(len(names)))
#         tmp_buffer = self.read_disk(names)

#         for i_name, name in enumerate(names):
#             if (self.pointer < self.buffer_size):
#                 self.index[name] = int(self.pointer)
#                 for arr, video in zip(self.buffer, tmp_buffer):
#                     arr[self.pointer, ...] = video[i_name, ...]
#                     self.pointer += 1
#             else:
#                 break

#         return(tmp_buffer, tmp_names)

#     def populate(self, names):

#         gigs = 2**30

#         if not hasattr(self, 'buffer'):
#             # Compute the size of a video to allocate buffer

#             imgs = self.read_disk(names[0:1])
#             tmp = [np.zeros((1, *np.squeeze(img).shape), dtype=np.float32)
#                    for img in imgs]
#             imgs_size = sum([t.nbytes for t in tmp])
#             self.buffer_size = int(
#                 np.floor((self.cache_size * gigs) / imgs_size))
#             # although pool says it can imap < 4 GB, 10 GB seem to work!
#             self.batchitersize = int(10 * gigs / imgs_size)

#             print('Allocating {} GB for {} data'.format(
#                 imgs_size * self.buffer_size / gigs,
#                 self.buffer_size))

#             self.buffer = [np.zeros(
#                 (self.buffer_size, *np.squeeze(img).shape), dtype=np.float32) for img in imgs]

#         if self.pointer < self.buffer_size:
#             sz_available = self.buffer_size - self.pointer
#             try:
#                 get_names = names[0:sz_available]
#             except:
#                 get_names = names[0:]
#         else:
#             print('Buffer is full')
#             return

#         if len(get_names) > self.batchitersize:
#             print('Multi-threading on {} batchsize for {} data'.format(self.batchitersize,
#                                                                        len(get_names)))
#             for i in range(0, len(get_names), self.batchitersize):
#                 if (len(get_names) - i) < self.batchitersize:
#                     batchitsz = len(get_names) - i
#                 else:
#                     batchitsz = self.batchitersize

#                 batch = get_names[i:i + batchitsz]
#                 print('{} to {}'.format(i, i + batchitsz))
#                 videos_batch = self.read_disk(batch)

#                 for arr, video in zip(self.buffer, videos_batch):
#                     arr[i:i + batchitsz] = video
#         else:
#             videos = self.read_disk(get_names)
#             self.buffer = videos

#         tmpseries = pd.Series(
#             list(range(self.pointer, self.pointer + len(get_names))), index=get_names)
#         self.index.update(tmpseries.to_dict())
#         self.pointer += len(get_names)

#         return

#     def read(self, names, pbar=True):
#         if type(names) is str:
#             names = [names, ]

#         if type(names) is tuple:
#             names = list(names)

#         saved_names = np.array([name in self.index.keys() for name in names])

#         if all(saved_names):
#             names_idx = [self.index[name] for name in names]
#             videos_out = [arr[names_idx, ...] for arr in self.buffer]
#         else:
#             not_saved = np.array(names)[np.where(saved_names == False)[0]]
#             tmp_buffer, tmp_names = self.push(not_saved)

#             videos_out = [np.zeros(
#                 (len(names), arr.shape[1], arr.shape[2]), dtype=np.float32) for arr in tmp_buffer]
#             for ni, (saved, name) in enumerate(zip(saved_names, names)):
#                 if saved:
#                     for video, arr in zip(videos_out, self.buffer):
#                         video[ni, ...] = arr[self.index[name], ...]
#                 else:
#                     for video, arr in zip(videos_out, tmp_buffer):
#                         video[ni, ...] = arr[tmp_names[name], ...]

#         return videos_out


# class AuxBroker_MI(MIBroker_multithreaded):
#     def __init__(self, aux_df, *args, **kwargs):
#         '''
#         reads video with auxiliary data, both with same index
#         '''
#         super().__init__(*args, **kwargs)
#         self.aux = aux_df

#     def read(self, names, pbar=False):
#         tmp = super().read(names)
#         tmp.append(self.aux.loc[names])
#         return(tmp)
