import numpy as np
from dask_ml.model_selection import GridSearchCV
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import roc_auc_score


def generate_folds(echo, n_folds=5, th=365, by='first',
                   seed=1988, validation_split=0):
    '''

    Generates training and testing fold indexes using
    first or last encounter as the stratifying label
    Input:
    - echo : dataframe with only one level column name
        - 'PT_SURVIVAL': should contain the currently known
                         survival time from the time the
                         measurement was taken
        - 'PT_MRN' : should contain the patient identifier
        - 'PT_STATUS': 1 for deceased, 0 for alive, anything else for unknown
        - 'TODAY_DT': Date the sample was measured
    - n_folds : number of folds
    - th : Threshold for survival prediction
    - by : ['first', 'last'] wheter use the status at the first
           or last encounter for stratification.
    - seed: random seed for reproducibility (Caution: it is best
      practice to save the folds instead of relying in the seed, as
      the results are not guaranteed to be the same by python )
    - validation_split: used to split the training fold for validation with balanced samples
    Output:
    - [(train0, test0), (train1, test1), ...]: list of tuples that contain
      train and test indexes
    - if 0<validation_split<1 returns (train0, val0,test0)
    '''

    assert 'PT_SURVIVAL' in echo, 'Did not find PT_SURVIVAL in dataframe'
    assert 'PT_MRN' in echo, 'Did not find PT_MRN in dataframe'
    assert 'TODAY_DT' in echo, 'No TODAY_DT in dataframe'
    assert 'PT_STATUS' in echo, 'Did not find PT_STATUS in dataframe'

    # Obtain valid samples given the threshold
    dead = (echo['PT_SURVIVAL'] <= th) & (echo['PT_STATUS'] == 'DECEASED')
    alive = echo['PT_SURVIVAL'] > th
    valid = dead | alive

    print('Found {} valid studies ({} patients): {} Dead, {} Alive'.format(
        sum(valid), echo['PT_MRN'][valid].nunique(), sum(dead), sum(alive)))

    # build parition form first/last encounter
    df_by_mrn = echo[valid]\
        .sort_values('TODAY_DT',
                     ascending=(by == by))\
        .groupby('PT_MRN')\
        .first()\
        .reset_index()\
        .set_index('PT_MRN')['PT_SURVIVAL']

    # function to map mrns to echo table index
    def mrn2echo(mrn_idx):
        mrns = df_by_mrn.iloc[mrn_idx].index.tolist()
        return np.where(echo['PT_MRN'].isin(mrns) & valid)[0]

    temp = df_by_mrn <= th
    kf = []
    skf = StratifiedKFold(n_splits=n_folds, shuffle=True,
                          random_state=seed)
    for k, (train, test) in enumerate(skf.split(np.zeros_like(temp), temp)):
        test_idx = mrn2echo(test)
        if validation_split == 0:
            val_idx = []
            train_idx = mrn2echo(train)
            kf.append((train_idx, test_idx))
        else:
            n_val = int(train.size * validation_split)
            y_train = temp.iloc[train]
            t0 = train[np.where(y_train == 0)[0]]
            idx0 = np.random.choice(t0, n_val // 2, replace=False)
            t1 = train[np.where(y_train == 1)[0]]
            idx1 = np.random.choice(t1, n_val // 2, replace=False)
            val = np.concatenate((idx0, idx1))
            # TODO: validation set is balanced at MRN level but not STUDY level
            train = np.array(list(set(train) - set(val)))
            train_idx = mrn2echo(train)
            val_idx = mrn2echo(val)
            kf.append((train_idx, val_idx, test_idx))

        print('Fold {}: train {} ({} dead), validation {} ({} dead), test {} ({} dead)'.format(
            k,
            len(train_idx),
            sum(dead.iloc[train_idx]),
            len(val_idx),
            sum(dead.iloc[val_idx]),
            len(test_idx),
            sum(dead.iloc[test_idx])))

    return kf, dead


def format_ypred(y_pred):

    if len(y_pred.shape) == 2:
        if y_pred.shape[1] == 2:
            y_pred = y_pred[:, 1]
        elif y_pred.shape[1] == 1:
            y_pred = np.squeeze(y_pred)

    return y_pred


def score(pipe, param_grid, X, y, kfold, n_jobs=1, y_weights=None, custom=None):
    '''
    Fit and scores a model on static data (EHR) given a pipeline defined as:

        pipe = pipeline.Pipeline([('scaler', preprocessing.StandardScaler()),
                                  ('features', PCA()),
                                  ('classifier', RandomForest())])

    Input:
    - pipe: sklearn defined pipeline
    - param_grid: dictionary with parameter definitions for every step of
                  the pipeline, keep empty for default parameters
    - X: all input data in observation by variable format
    - y: labels for each row of X
    - kfold: list of tuples containing train and test indexes for X and y
    - n_jobs: number of concurrent processes to run. Keep in default of 1
              if the model already uses multiprocessing.
    - y_weights: sample weight, same size as y
    - custom: custom function to apply to best classifier
    Output:
    - aucs: list of aucs for each fold in kfolds
    - fimps: feature importance if available from classifier in model

    '''
    results = {'AUCs': [],
               'FIMPs': [],
               'Predictions': np.zeros(len(y)),
               'Custom': []}
    for k, (train, test) in enumerate(kfold):

        if type(X) is list:
            x_train = [i[train] for i in X]
            x_test = [i[test] for i in X]
        else:
            x_train, x_test = X[train, :], X[test, :]

        y_train, y_test = y[train], y[test]

        if y_weights is not None:
            sample_weight = y_weights[train]
        else:
            sample_weight = None

        if param_grid:
            clf = GridSearchCV(estimator=pipe,
                               param_grid=param_grid, n_jobs=n_jobs,
                               scoring='roc_auc', cv=5)
        else:
            clf = pipe

        clf.fit(x_train, y_train, classifier__sample_weight=sample_weight)

        if param_grid:
            print(clf.best_params_)

        y_pred = format_ypred(clf.predict_proba(x_test))

        if len(y_pred.shape) == 2:
            if y_pred.shape[1] == 2:
                y_pred = y_pred[:, 1]
            elif y_pred.shape[1] == 1:
                y_pred = np.squeeze(y_pred)

        results['Predictions'][test] = y_pred
        y_train_pred = format_ypred(clf.predict_proba(x_train))
        r_train_auc = roc_auc_score(y_true=y_train, y_score=y_train_pred)
        rauc = roc_auc_score(y_true=y_test, y_score=y_pred)
        print('Fold %d:Train %.3f, Test %.3f' % (k, r_train_auc, rauc))

        results['AUCs'].append(rauc)

        if param_grid:
            # Get feature importance from best estimator
            imp = clf.best_estimator_.steps[-1][-1]
        else:
            imp = clf.steps[-1][-1]

        if custom is not None:
            results['Custom'].append(custom(imp))

        if hasattr(imp, 'feature_importances_'):
            results['FIMPs'].append(imp.feature_importances_)
        elif hasattr(imp, 'coef_'):
            results['FIMPs'].append(imp.coef_)
        else:
            results['FIMPs'].append(None)

    mauc = np.mean(results['AUCs'])
    sauc = np.std(results['AUCs'])

    print('AUC: %.2f (+- %.2f)' % (mauc, sauc))

    return results
