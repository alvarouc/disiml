from itertools import islice
from typing import List, Callable
import tensorflow as tf
from tensorflow.keras.models import model_from_json, Model
from tensorflow.keras.callbacks import (EarlyStopping, ModelCheckpoint)
from tensorflow.keras import backend as K
from tensorflow.keras.optimizers import Adam, RMSprop, SGD, Adagrad, Adadelta
from tqdm import tqdm
import numpy as np
import sklearn
import random
import tempfile
import warnings


tf.compat.v1.disable_eager_execution()

def write_log(callback, names, logs, batch_no):
    for name, value in zip(names, logs):
        summary = tf.Summary()
        summary_value = summary.value.add()
        summary_value.simple_value = value
        summary_value.tag = name
        callback.writer.add_summary(summary, batch_no)
        callback.writer.flush()


def grouped(iterable, n):
    it = iter(iterable)
    return iter(lambda: tuple(islice(it, n)), ())


class BasePredictor():
    '''
    Base predictor class for scikit-learn compatibility

    Parameters
    ----------
    build_fun
        Callable that returns a keras model, assuming it gets input_shape and 
        optimizer paramenters
    optimizer: str
        passed to `build_fun` as a Tensorflow optimizer object
    learning_rate: float
        passed to `build_fun`
    loss: str
        passed to `build_fun`
    build_args : dict
        Extra parameters for build_fun
    class_weight: dict
        Dictionary that maps the labels and weights. For example, 
        ``class_weight = {0: 0.1, 1:1}``
    batch_size: int
        Number of samples per batch. Ignored when using `fit_generator`
    epochs: int
        Maximum number of epochs
    patience: int
        If validation data is passed to the `fit`, `fit_batches`, or `fit_generator` 
        then stops training after the loss metric in the validation set has not increased 
        for `patience` epochs
    keep_best: bool
        If validation data is passed to the `fit`, `fit_batches`, or `fit_generator` 
        then it loads the model at the epoch with best alidation scores
    verbose: bool
        Prints a variety of information including model summary
    experiment: str
        Experiment name for model checkpoint saving
    regression: bool
        Build a regressor
    metrics: list(str)
        List of metrics to log
    checkpoint: int
        Number of epochs to wait for writing checkpoints. If ``checkpoint=0`` 
        then no checkpoint is saved.
    extra_callbacks: keras.Callback
        List of custom keras callbacks
    '''

    def __init__(
        self, 
        build_fun       : Callable, 
        build_args      : dict      ={},
        optimizer       : str       ='Adam',
        learning_rate   : float     = None,
        loss            : str       ='binary_crossentropy',
        class_weight    : dict      =None,
        batch_size      : int       =100,
        epochs          : int       =100,
        patience        : int       =10,
        keep_best       : bool      =True,
        verbose         : bool      =True,
        experiment      : str       ='experiment',
        regression      : bool      =False,
        metrics         : List[str] =['acc', 'auc'],
        checkpoint      : int       =0,
        batch_augmentor : Callable  =None,
        callbacks       : list      =[]):
        
        # Model args
        self.build_fun      = build_fun
        self.build_args     = build_args
        self.metrics        = metrics
        self.loss           = loss
        self.optimizer      = optimizer
        self.verbose        = verbose

        metrics_list = []
        if 'auc' in metrics:
            metrics_list.append(tf.keras.metrics.AUC())
        if 'acc' in metrics:
            metrics_list.append(tf.keras.metrics.BinaryAccuracy())

        self.model_args = dict(
            loss = self.loss,
            optimizer=self.optimizer,
            metrics=metrics_list,
            verbose=self.verbose, 
            **self.build_args)

        # Fit args
        self.batch_size     = batch_size
        self.class_weight   = class_weight
        self.epochs         = epochs
        self.callbacks      = callbacks

        self.fit_args = dict(
            batch_size= self.batch_size,
            class_weight= self.class_weight,
            epochs= self.epochs,
            callbacks= [],
            #callbacks= self.callbacks,
            verbose= self.verbose)

        # Output auto-config
        self.regression     = regression

        # Pre-set Callbacks
        self.patience       = patience
        self.checkpoint     = checkpoint
        self.keep_best      = keep_best
        self.experiment     = experiment
        self.batch_no       = 0

        # internals
        self.history        = None
        self.fitted         = False  # flag to check if model was fitted previously

        # Deprecated
        self.batch_augmentor= batch_augmentor 

        # Set optimizer
        self.learning_rate  = learning_rate
        # optim_params = {}
        # if self.learning_rate is not None:
        #     optim_params['lr'] = self.learning_rate
        # optim_params_str = ','.join([f'{key}={val}' for key,val in optim_params.items()])
        self.optimizer = self.optimizer
        #try:
            #exec(f'from tensorflow.python.keras.optimizers import {optimizer}')
            #self.optimizer = eval(f'{optimizer}()')

        #except:
        #   raise 'Error: optimizer not found'
            

    def log(self, loss, names=['Loss', 'Accuracy']):
        self.batch_no += 1
        write_log(callback=self.callback,
                  names=names,
                  logs=loss,
                  batch_no=self.batch_no)


    def _prepare_fit_args(self, X, y,
                          sample_weight=None, 
                          weight_classes=False,
                          validation_data=None,
                          monitor='val_loss'):

        # Get input shape and check y 
        self._build_in(X)
        self._build_out(y)

        # Callbacks with validation
        if (validation_data is not None) and (self.patience > 0):
            
            self.fit_args['callbacks'].append(
                EarlyStopping(
                    monitor=monitor,
                    min_delta=0,
                    patience=self.patience,
                    verbose=self.verbose,
                    restore_best_weights=self.keep_best,
                    mode='auto'))
           
        if self.checkpoint > 0:
            self.fit_args['callbacks'].append(
                ModelCheckpoint(
                    self.experiment + '_weights.{epoch:02d}-{loss:.2f}.hdf5',
                    save_freq=self.checkpoint))

        self.fit_args['sample_weight'] = sample_weight
        # Weird behavior of callbacks being passed across instances
        # extending it fixes it. I still don't understand why.
        self.fit_args['callbacks'].extend(self.callbacks) 

        if not self.class_weight and weight_classes:
            self._compute_weights(y)

        self.model = self.build_fun(**self.model_args)
        # Forcing learning rate as passing with parameters fail
        # See https://github.com/tensorflow/tensorflow/issues/31324
        if self.learning_rate:
            self.model.optimizer.lr = self.learning_rate
        if self.verbose: 
            self.model.summary()

    def fit(self, X, y=None, validation_data=None,
            sample_weight=None, weight_classes=True, 
            monitor='val_loss', **kwargs):
        '''
        Generic fit function. Use when data `X` fits in ram.

        Parameters
        ----------
            X : np.ndarray or tf.keras.utils.Sequence
                data in format (samples, features)
            y : np.ndarray 
                labels in format (samples, classes)
            validation_data : tuple(np.ndarray)
                Tuple that contains validation data in (`X`, `y`) format
            sample_weight: np.ndarray
                Sample weigths in format (samples, )
            monitor: str
                options are ['val_loss', 'val_auc']
            weight_classes: bool
                Recomputes weight classes given `y`

        Returns
        -------
            history
                Keras history object

        '''

        if not self.fitted:
            if isinstance(X, tf.keras.utils.Sequence):
                X_prefit, y_prefit = X[0]
                self.fit_args['batch_size'] = None 

            else:
                X_prefit, y_prefit = X, y
            
            self._prepare_fit_args(
                X_prefit, y_prefit,
                validation_data=validation_data,
                sample_weight=sample_weight, 
                weight_classes=weight_classes,
                monitor=monitor)

        self.history = self.model.fit(X, y, validation_data=validation_data, **self.fit_args, **kwargs)
        self.fitted = True
        return self


    def fit_batches(self, broker, train, val,  y, batch_augmentation=None):
        '''
        Caution
        ----------
            Kept for backwards compatibility, use `fit_generator` instead

        Use to fit when train data does not fit on ram. 
    
        Parameters
        ----------
            broker: [*]broker
                Broker object that implements `read` method
            train: list()
                list of train sample ids
            val: list()
                list of val sample ids
            y: pd.Series
                Pandas Series indexed with sample ids with label values
            batch_augmentation: generator,
                Receives a function to augment batches produced by broker.
        '''

        if not hasattr(broker, 'data_shape'):
            broker.read(str(train[0]))
        self._prepare_fit_args(broker, y)
        all_sw = self.fit_args['sample_weight']

        def val_batch_idxs():
            return tqdm(enumerate(grouped(val, self.batch_size)),
                        total=len(val) // self.batch_size, desc='Val_Batch')

        def train_batch_idxs():
            # shuffle train samples every epoch
            shuffled = list(train).copy()
            random.shuffle(shuffled)
            return tqdm(enumerate(grouped(shuffled, self.batch_size)),
                        total=len(train) // self.batch_size, desc='Train_Batch')

        # Since train in shuffled above, 'populate' does not ensure that
        # first x batches will have the data loaded completely in memory
        # to make the most of it. Possible improvemnet later.

        # broker.read(np.concatenate((train, val)))
        

        metrics_names = ['unknown' for x in range(len(self.metrics) + 1)]
        if self.keep_best:
            temp_model_file = tempfile.NamedTemporaryFile(
                mode='w+b', delete=False)
            print('fit_batches: using temp file for best val_loss: {}'.format(
                temp_model_file.name))

        min_val_loss = 1e3
        patience = 0
        for epoch in range(self.epochs):
            #             loss = 0
            train_results = np.zeros(len(metrics_names))
            val_results = np.zeros(len(metrics_names))

            for n, idxs in train_batch_idxs():

                # Grab videos, EHR, and labels
                X_b = broker.read(list(idxs))
                y_b = y.loc[list(idxs)]

                if not self.fitted:
                    metrics_names = self.model.metrics_names
                    print('fit_batches: metrics_names init to {}'.format(
                        metrics_names))
                    self.fitted = True

                if all_sw is None:
                    sw = None
                else:
                    sw = all_sw[list(idxs)]
                # Train on batch
                if batch_augmentation is not None:
                    for x_n, y_n in batch_augmentation(X_b, y_b):
                        train_results += self.model.train_on_batch(
                        x=x_n, y=y_n,
                        sample_weight=sw,
                        class_weight=self.fit_args['class_weight'])
                else:
                    train_results += self.model.train_on_batch(
                        x=X_b, y=y_b,
                        sample_weight=sw,
                        class_weight=self.fit_args['class_weight'])

            if (self.checkpoint > 0) and ((epoch % self.checkpoint) == 0):
                self.save(self.experiment +
                          '_weights.{:03d}-{:.3f}.hdf5'.format(epoch, train_results[0]))

            for nn, idx in val_batch_idxs():
                val_results += np.multiply(len(idx), self.model.test_on_batch(
                    broker.read(list(idx)), y.loc[list(idx)]))

            val_results /= len(val)
            # First metric is always loss.
            if val_results[0] >= min_val_loss:
                patience += 1
            else:
                min_val_loss = val_results[0]
                patience = 0
                # Save this new best model
                if self.keep_best:
                    print(
                        'fit_batches: saving model weights with best-so-far val_loss {}.'.format(val_results[0]))
                    # self.model.save_weights(temp_model_file.name)
                    self.save(temp_model_file.name)

            # Report all metrics that the model returns, remembering that the training
            # results are summed over all batches, while the val_results have already
            # been properly normalized.
            print('Epoch {}, patience={}:'.format(
                epoch, patience))
            for metric, tscore, vscore in zip(
                    metrics_names, train_results, val_results):
                print('\t{} train/val: {:.3f}--{:.3f}'.format(metric,
                                                              tscore /
                                                              (n + 1),
                                                              vscore),
                      end='\t')
            print('')  # new line after printing scores
            if patience >= self.patience:
                print('...Breaking on patience...')
                break

        if self.keep_best:
            print('fit_batches: reloading best model weights from {}.'.format(
                temp_model_file.name))
            self.load(temp_model_file.name)

        self.fitted = True

        return self

    def predict_proba(self, X, y=None, **kwargs):
        '''
        Predict scores from input data. It supports numpy arrays and generator objects

        Parameters
        ----------
            X
                data to predict from
            y
                Not used, kept for sklearn compatibility
            **kwargs
                Additional parameters passed to predict method

        Returns
        -------
            y: np.ndarray
                Prediction scores
        '''
        return self._predict(X, y=y, **kwargs)

    def predict(self, X, y=None, **kwargs):
        return self._predict(X, y=y, **kwargs)

    def _predict(self, X, y=None, **kwargs):
        assert self.fitted, 'Run fit method first'
        if type(X) is np.ndarray or type(X) is list:
            y = self.model.predict(X, batch_size=self.batch_size, verbose=self.verbose, **kwargs)
        elif isinstance(X, tf.keras.utils.Sequence):
            y = self.model.predict(X, **kwargs)
        return y

    def save(self, name='temp_name'):
        '''
        Saves model definition to a json file and model weights to hdf5 file

        Parameters
        ----------
            name: str
                Name of the model to save

        '''
        # serialize model to JSON
        model_json = self.model.to_json()
        with open(name + '.json', "w") as json_file:
            json_file.write(model_json)
        # serialize weights to HDF5
        self.model.save_weights(name + '.h5')
        return self

    def load(self, name='temp_name'):
        '''
        Loads model definition, as saved by `save` method, from a json 
        file and model weights from hdf5 file 

        Parameters
        ----------
            name: str
                Name of the model to load
        '''
        # load json and create model
        with open(name + '.json', 'r') as json_file:
            loaded_model_json = json_file.read()

        loaded_model = model_from_json(loaded_model_json)
        # load weights into new model
        loaded_model.load_weights(name + '.h5')
        self.model = loaded_model
        self.fitted = True

    def predict_batches(self, broker, test):
        y_pred = []
        for n, idxs in tqdm(enumerate(grouped(test, self.batch_size)),
                            total=len(test) // self.batch_size, desc='Predict_Batch'):
            # Grab videos, EHR, and labels
            X = broker.read(list(idxs))
            y_pred.extend(list(self.model.predict_on_batch(
                X).ravel().tolist()))

        return np.array(y_pred)


class SI(BasePredictor):
    '''
    SI or Single Input class that implements the build_in method to compute `input_shape` parameter for `build_fun`  

    Important
    ---------
    It supports numpy arrays, broker and generator objects that implement the `data_shape` attribute 

    '''

    def _build_in(self, X):
        if hasattr(X, 'shape'):
            self.model_args['input_shape'] = X.shape[1:]
        elif hasattr(X, 'data_shape'):
            self.model_args['input_shape'] = X.data_shape

    def transform(self, X, layer_name):
        '''
        Sklearn-like transform function.

        Given a `layer_name` it will return the layer output 

        Parameters
        ----------
            X: np.ndarray
                Data to transform, supports numpy arrays only
            layer_name: str
                Name of the layer as defined in the `build_fun`

        Returns
        -------
            y: np.ndarray
                Output of `layer_name`
                
        '''
        assert self.fitted, 'Run fit method first'

        output = self.model.get_layer(layer_name).output

        inp = self.model.input  # input placeholder
        functor = K.function([inp] + [K.learning_phase()], [output])
        y = functor([X, 0.])[0]
        return y

class MI(BasePredictor):
    '''
    MI or Multiple Input class that implements the build_in method to compute `input_shape` parameter for `build_fun`  as a list of tuples.

    Important
    ---------
    MI supports lists of numpy arrays, list of brokers, or a generator that implement the `data_shape` attribute 

    '''

    def _build_in(self, X):

        if hasattr(X, 'data_shape'):

            self.model_args['input_shape'] = X.data_shape

        else:
            assert type(X) is list, 'MI: Input must be a list'
            samples = np.array([a.shape[0] for a in X])
            assert np.all(samples == samples[0]), \
            'First dimension should be the same for all inputs'
            self.model_args['input_shape'] = [Xi.shape[1:] for Xi in X]



    def transform(self, X, layer_name):
        '''
        Sklearn-like transform function.

        Given a `layer_name` it will return the layer output 

        Parameters
        ----------
            X: list(np.ndarray)
                Data to transform, supports list of numpy arrays only
            layer_name: str
                Name of the layer as defined in the `build_fun`

        Returns
        -------
            y: np.ndarray
                Output of `layer_name`
                
        '''
        assert self.fitted, 'Run fit method first'
        output = self.model.get_layer(layer_name).output

        inp = self.model.input  # input placeholder
        intermediate_layer_model = Model(inputs=inp, outputs=output)
        outimg = intermediate_layer_model.predict(X)

        return outimg


class SO(BasePredictor):

    def _compute_weights(self, y):

        if len(y.shape) == 1 or y.shape[1]==1:
            uniq_y = np.unique(y)
            c_ws = sklearn.utils.class_weight.compute_class_weight(class_weight='balanced',classes=uniq_y, y=y.ravel())
            self.fit_args['class_weight'] = {
                n: w for n, w in zip(uniq_y, c_ws)}
        else: # y multi class
            self.fit_args['class_weight'] = {
                n: w for n, w in zip(range(y.shape[1]), 1-y.mean(axis=0))}


    def _build_out(self, y):

        y = np.squeeze(y)

        if len(y.shape)==1:
            self.model_args['num_class'] = 1 
        else:
            self.model_args['num_class'] = y.shape[1]

        if len(y.shape) == 1 and len(np.unique(y)) > 2 and self.regression == False:

            raise Exception(
                'multi-label y should be hot-encoded (Use: keras.utils.to_categorical)')

        if self.loss is None:
            self.loss = 'binary_crossentropy'
            if len(y.shape) > 1:
                print('default loss (binary_crossentropy) used for multi-label')



class MO(BasePredictor):

    def _build_out(self, y):

        num_class = []
        for yi in y:
            yi = np.squeeze(yi)
            nc = len(np.unique(yi)) if len(yi.shape) == 1 else yi.shape[1]

            num_class.append(nc)
            if len(yi.shape) == 1 and len(np.unique(yi)) != 2 and self.regression == False:
                raise Exception(
                    'multi-label y should be hot-encoded (Use: keras.utils.to_categorical)')
            assert nc != 1, 'y cannot have single label'

        self.model_args['num_class'] = num_class

        loss_list = []
        for yi in y:
            loss_list.append('binary_crossentropy')
            if len(yi.shape) > 1:
                print('default loss (binary_crossentropy) used for multi-label')

        self.loss = loss_list

    def _compute_weights(self, y):

        assert type(y) is list, 'y should be a list of labels'

        c_ws = []
        out_names = []
        for ind, ytmp in enumerate(y):
            ytmp = np.squeeze(ytmp)
            out_names.append('main_output' + str(ind))
            yt = ytmp if len(ytmp.shape) == 1 else np.argmax(ytmp, axis=1)
            uniq_y = np.unique(yt)
            c_ws_tmp = sklearn.utils.class_weight.compute_class_weight(
                'balanced', uniq_y, yt.ravel())
            c_ws.append({n: w for n, w in zip(uniq_y, c_ws_tmp)})
        self.fit_args['class_weight'] = {
            n: w for n, w in zip(out_names, c_ws)}




