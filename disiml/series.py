from tensorflow.python import keras
from tensorflow.python.keras.models import Model
from tensorflow.python.keras.layers import (
    Dense, Dropout, LSTM, Conv1D,
    Input, concatenate, Flatten)
from tensorflow.python.keras.backend import floatx
from .predictor import SI, SO, MI, MO
from .ae import build_1dcnn_encoder


def build_siso(input_shape, loss, optimizer, metrics=None, num_class=1, **build_args):

    conv_args = dict(activation='relu', padding='same')
    input_series = Input(shape=input_shape)
    x = build_1dcnn_encoder(input_series, conv_args)
    x = Flatten()(x)
    x = Dense(32, activation='sigmoid')(x)
    x = Dropout(0.5)(x)
    if num_class > 1:
        output_node = Dense(
            num_class, activation='softmax', name='main_output')(x)
    else:
        output_node = Dense(
            1, activation='sigmoid', name='main_output')(x)

    model = Model(inputs=input_series, outputs=output_node)
    model.compile(loss=loss,optimizer=optimizer, metrics=metrics)

    return model
# def build_siso(input_shape, num_class, lstm_units=128,
#                optimizer='rmsprop', verbose=True):

#     main_input = Input(shape=input_shape, dtype=floatx(),
#                        name='main_input')
#     lstm_out = LSTM(lstm_units, name='lstm_output')(main_input)
#     x = Dense(32, activation='sigmoid')(lstm_out)
#     x = Dropout(0.5)(x)
#     if num_class > 2:
#         main_output = Dense(
#             num_class, activation='softmax', name='main_output')(x)
#     else:
#         main_output = Dense(1, activation='sigmoid', name='main_output')(x)

#     model = Model(inputs=main_input, outputs=main_output)
#     return model


def build_simo(input_shape,  loss, optimizer, metrics=None, num_class=2, lstm_units=128,
               verbose=True):

    main_input = Input(shape=input_shape, dtype=floatx(),
                       name='main_input')
    lstm_out = LSTM(lstm_units, name='lstm_output')(main_input)
    x = Dense(32, activation='sigmoid')(lstm_out)
    x = Dropout(0.5)(x)

    # num_class will be a list for MO
    main_output = []
    for ind, nc in enumerate(num_class):
        if nc > 2:
            main_output.append(
                Dense(nc, activation='softmax', name='main_output' + str(ind))(x))
        else:
            main_output.append(Dense(1, activation='sigmoid',
                                     name='main_output' + str(ind))(x))

    model = Model(inputs=main_input, outputs=main_output)
    model.compile(loss=loss,optimizer=optimizer, metrics=metrics)

    return model


def build_miso(input_shape, loss, optimizer, metrics=None, num_class=2, lstm_units=128,
               verbose=True):
    '''
    Builds a Keras model for prediction with time series
    Inputs:
    - input_shape : (n_series, length) (# of features or time series, length of the series)
    - num_class: number of classes to predict    
    - lstm_units: number of hidden units in LSTM layer
    - aux_shape: shape of auxiliary data (static EHR)
    - verbose: verbosity flag
    Output:
    - model: keras model to fit and predict
    '''
    concat_layers = []
    main_inputs = []

    if len(input_shape[-1]) == 1:
        aux_shape = input_shape[-1]
        del input_shape[-1]
    else:
        aux_shape = None

    for ind, in_shape in enumerate(input_shape):
        main_input = Input(shape=in_shape,
                           dtype=floatx(),
                           name='main_input' + str(ind))
        main_inputs.append(main_input)

        lstm_out = LSTM(lstm_units, name='lstm_output' + str(ind))(main_input)
        concat_layers.append(lstm_out)

    if aux_shape is not None:  # concatenate aux data
        auxiliary_input = Input(shape=aux_shape, name='aux_input')
        main_inputs.append(auxiliary_input)
        aux_out = Dense(32, activation='sigmoid')(auxiliary_input)
        concat_layers.append(aux_out)

    x = concatenate(concat_layers, name='concat_output')
    x = Dense(32, activation='sigmoid')(x)

    if num_class > 2:
        main_output = Dense(
            num_class, activation='softmax', name='main_output')(x)
    else:
        main_output = Dense(1, activation='sigmoid', name='main_output')(x)

    model = Model(inputs=main_inputs,
                  outputs=main_output)
    model.compile(loss=loss,optimizer=optimizer, metrics=metrics)

    return model


def build_mimo(input_shape, loss, optimizer, metrics=None, num_class=2,  aux_shape=None, lstm_units=128,
               verbose=True):
    '''
    Builds a Keras model for prediction with time series
    Inputs:
    - input_shape : (n_series, length) (# of features or time series, length of the series)
    - num_class: number of classes to predict    
    - lstm_units: number of hidden units in LSTM layer
    - aux_shape: shape of auxiliary data (static EHR)
    - optimizer: optimization algorithm
    - learning_rate : learning rate for the optimizer
    - verbose: verbosity flag
    Output:
    - model: keras model to fit and predict
    '''
    concat_layers = []
    main_inputs = []

    if len(input_shape[-1]) == 1:
        aux_shape = input_shape[-1]
        del input_shape[-1]
    else:
        aux_shape = None

    for ind, in_shape in enumerate(input_shape):
        main_input = Input(shape=in_shape,
                           dtype=floatx(),
                           name='main_input' + str(ind))
        main_inputs.append(main_input)

        lstm_out = LSTM(lstm_units, name='lstm_output' + str(ind))(main_input)
        concat_layers.append(lstm_out)

    if aux_shape is not None:  # concatenate aux data
        auxiliary_input = Input(shape=aux_shape, name='aux_input')
        main_inputs.append(auxiliary_input)
        aux_out = Dense(32, activation='sigmoid')(auxiliary_input)
        concat_layers.append(aux_out)

    x = concatenate(concat_layers, name='concat_output_aux')
    x = Dense(32, activation='sigmoid')(x)

    # num_class will be a list for MO
    main_output = []
    for ind, nc in enumerate(num_class):
        if nc > 2:
            main_output.append(
                Dense(nc, activation='softmax', name='main_output' + str(ind))(x))
        else:
            main_output.append(Dense(1, activation='sigmoid',
                                     name='main_output' + str(ind))(x))

    model = Model(inputs=main_inputs, outputs=main_output)
    model.compile(loss=loss,optimizer=optimizer, metrics=metrics)

    return model


class SeriesSISO(SI, SO):
    '''
    Class that implements a time series predictor model
    that is sklearn compatible

    Example::

        from disiml import SeriesSISO
        import numpy as np
        X = np.random.random((1000,100,10))
        y = np.random.uniform(low=0, high=1, size=(1000, 1)) > .5

        clf = SeriesSISO()
        clf.fit(X,y)
        y_pred = clf.predict(X)

    '''

    def __init__(self, build_fun=build_siso,
                 lstm_units=16,
                 *args, **kwargs):
        super().__init__(build_fun=build_fun,
                         *args, **kwargs)
        self.lstm_units = lstm_units
        self.model_args['lstm_units'] = self.lstm_units


class SeriesSIMO(SI, MO):
    '''
    Class that implements a time series predictor model
    that is sklearn compatible

    Example::

        from disiml import SeriesSIMO
        import numpy as np
        X = np.random.random((1000,100,10))
        y = np.random.uniform(low=0, high=1, size=(1000, 1)) > .5
        ys = [y, 1-y]

        clf = SeriesSIMO()
        clf.fit(X,ys)
        y_preds = clf.predict(X)
    '''

    def __init__(self, build_fun=build_simo,
                 lstm_units=16,
                 *args, **kwargs):
        super().__init__(build_fun=build_fun,
                         *args, **kwargs)
        self.lstm_units = lstm_units
        self.model_args['lstm_units'] = self.lstm_units


class SeriesMISO(MI, SO):
    '''
    Class that implements a list of time series predictor model
    that is sklearn compatible

    Example::

        from disiml import SeriesMISO
        import numpy as np
        X = [
            np.random.random((1000,100,10)),
            np.random.random((1000,100,10))
            ]
        y = np.random.uniform(low=0, high=1, size=(1000, 1)) > .5

        clf = SeriesMISO()
        clf.fit(X,y)
        y_pred = clf.predict(X)

    '''

    def __init__(self, build_fun=build_miso,
                 lstm_units=16,
                 *args, **kwargs):
        super().__init__(build_fun=build_fun,
                         *args, **kwargs)
        self.lstm_units = lstm_units
        self.model_args['lstm_units'] = self.lstm_units


class SeriesMIMO(MI, MO):
    '''
    Class that implements a time series predictor model
    that is sklearn compatible

    Example::

        from disiml import SeriesMIMO
        import numpy as np
        X = [
            np.random.random((1000,100,10)),
            np.random.random((1000,100,10))
            ]
        y = np.random.uniform(low=0, high=1, size=(1000, 1)) > .5
        ys = [y, 1-y]
        
        clf = SeriesMISO()
        clf.fit(X,ys)
        y_pred = clf.predict(X)

    '''

    def __init__(self, build_fun=build_mimo,
                 lstm_units=16,
                 *args, **kwargs):
        super().__init__(build_fun=build_fun,
                         *args, **kwargs)
        self.lstm_units = lstm_units
        self.model_args['lstm_units'] = self.lstm_units


