import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras.layers import (Input, Dense, Activation,
                                            concatenate, Add, Multiply, Lambda)
from tensorflow.keras.regularizers import l1, l2
from tensorflow.keras.constraints import NonNeg
import numpy as np
import pandas as pd
import pickle
from .predictor import SI, SO, MI
from .broker import FileGenerator


def build_siso(input_shape, loss, optimizer, metrics=None, num_class=None,
               *args, **kwargs):
    '''
     Builds a Keras model for prediction from EHR
     Inputs:
     - input_shape: ( ncols, )
     Output:
     - model: keras model to fit and predict
     '''
    ehr_input = Input(shape=input_shape, name='ehr_input')
    x = Dense(100, activation='sigmoid')(ehr_input)
    x = Dense(50, activation='sigmoid')(x)
    x = Dense(10, activation='sigmoid')(x)
    if num_class is None:
        output = Dense(1, activation='sigmoid')(x)
    else:
        output = Dense(num_class, activation='softmax')(x)
    model = Model(inputs=ehr_input, outputs=output)
    model.compile(loss=loss,optimizer=optimizer, metrics=metrics)

    return model


class TabSISO(SI, SO):
    '''
    Class that implements a tabular predictor model
    that is sklearn compatible

    Example::

        from disiml import TabSISO
        import numpy as np
        X = np.random.random((1000,10))
        y = np.random.uniform(low=0, high=1, size=(1000, 1)) > .5

        clf = TabSISO()
        clf.fit(X,y)
        y_pred = clf.predict(X)

    '''

    def __init__(self, build_fun=build_siso,
                 *args, **kwargs):
        super().__init__(build_fun=build_fun,
                         *args, **kwargs)

def expand_column(x, order):
    exp = [(x**(ii))[:, None] for ii in range(1, order + 1)]
    return np.concatenate(exp, axis=1)


def expand(X_in, orders):

    if isinstance(X_in, pd.DataFrame):
        X = X_in.values
    elif isinstance(X_in, np.ndarray):
        X = X_in
    else:
        raise 'Data type not supported'      

    X2 = []
    for n, order in enumerate(orders):
        x = X[:, n]
        if order == 1:
            X2.append(x[:, None])
        else:
            exp = expand_column(x, order)
            X2.append(exp)

    return X2

# Generator that formats 
class INNGenerator(FileGenerator):
    '''
    Data generator for TabiMISO classifier
        This generator expands the auxiliary data in `aux` to be compatible with
        the polynomial expansion.

    Parameters
    ----------
        orders: list
            List of polynomial orders. Set one per aux feature

    Returns
    -------
        Data:
            List of modals and the expanded `aux` data 
    '''
    def __init__(self, orders, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.orders = orders

    def __getitem__(self, index):
        X, y = super().__getitem__(index)
        return X[:-1] + expand(X[-1], self.orders), y

def build_imiso(input_shape, num_class, loss, optimizer, 
                metrics=None,
                build_modals=[],
                interactions=False,
                verbose=True,
                cont_params=dict(kernel_constraint=NonNeg(),
                                 kernel_initializer='zeros'),
                cat_params=dict(kernel_initializer='zeros'),
                interaction_params=dict(kernel_initializer='zeros',
                                        kernel_regularizer=l1(0.1))):
    '''
    Builds a Keras model for prediction with time series
    Inputs:
    - input_shape : [modal_shape, (poly1_degree,), (poly2_degree,),...]
    - verbose: verbosity flag
    - Interactions: 
     -- True: uses all possible interactions
     -- False: no interactions
     -- List of tuples [(i,j),] : Will force interactions on the given tuple indices 
    Output:
    - model: keras model to fit and predict
    '''

    main_inputs = []  # contains modal inputs and expansions
    poly_outputs = []

    for build_modal in build_modals:
        temp = build_modal(input_shape=input_shape.pop(0))
        main_inputs.append(temp.input)
        poly_outputs.append(temp.output)

    linear_outputs = []
    tab_outputs = []
    for ind, in_shape in enumerate(input_shape):

        # if polynomial degree > 1
        if in_shape[0] != 1:
            a_in = Input(shape=in_shape,
                         name='poly_input_%s' % ind)
            a_out = Dense(1, activation='sigmoid',
                          name='poly_%s' % ind,
                          kernel_initializer='zeros')(a_in)
            poly_outputs.append(a_out)
        # else direct connection
        else:
            a_in = Input(shape=in_shape,
                         name='linear_input_%s' % ind)
            a_out = Lambda(lambda x: x)(a_in)
            linear_outputs.append(a_in)

        main_inputs.append(a_in)
        tab_outputs.append(a_out)

    polys_tensor = concatenate(poly_outputs, name='poly_concat')
    polys_dense = Dense(1, activation='linear',
                        name='beta_polys',
                        **cont_params)(polys_tensor)

    to_add = [polys_dense, ]

    if linear_outputs:
        linear_tensor = concatenate(linear_outputs, name='linear_concat')
        linear_dense = Dense(1, activation='linear',
                             use_bias=False,
                             name='beta_linears',
                             **cat_params)(linear_tensor)

        to_add.append(linear_dense)

    # Calculate interaction terms
    if interactions:
        interaction_outputs = []

        if type(interactions) is list:

            for iis in interactions:
                to_mul = [tab_outputs[ii] for ii in iis]
                inter_out = Multiply()(to_mul)
                interaction_outputs.append(inter_out)

        elif type(interactions) is bool:
            n_tab = len(input_shape)
            for i in range(n_tab):
                for j in range(i + 1, n_tab):
                    inter_out = Multiply()([tab_outputs[i], tab_outputs[j]])
                    interaction_outputs.append(inter_out)

        interaction_tensor = concatenate(interaction_outputs,
                                         name='interactions')
        interactions_dense = Dense(1, activation='linear',
                                   use_bias=False,
                                   name='gammas',
                                   **interaction_params)(interaction_tensor)
        to_add.append(interactions_dense)

    if len(to_add) > 1:
        x = Add()(to_add)
    else:
        x = to_add[0]

    main_output = Activation('sigmoid')(x)

    model = Model(inputs=main_inputs,
                  outputs=main_output)
    model.compile(loss=loss,optimizer=optimizer, metrics=metrics)

    return model


class TabiMISO(MI, SO):
    """
    Interpretable neural network with multi-modal input
    
    Note
    ----
        Asumes the first inputs are the non-tabular data modalities

    Parameters
    ----------
        orders
            polynomial orders in a list
        build_modals
            callables to build the model for modes
        interactions
            True or list of pairs
        pretrained_ws
            name of previously saved model with `saveINN method`

    Example::

        from disiml import TabiMISO
        import numpy as np
        X = [
            np.random.random((1000,100,100,10, 10)), # 1000 videos
            np.random.random((1000,10)) # 1000 tabular features
            ]
        y = np.random.uniform(low=0, high=1, size=(1000, 1)) > .5

        def build_model(input_shape,
                        video_model_path,
                        *args, **kwargs):
            # Load pretrained Video model
            with open(video_model_path + '.json', 'r') as json_file:
                video_model = model_from_json(json_file.read())
            video_model.load_weights(video_model_path + '.h5')
            return video_model

        orders = [1, 3, 3, 3, 5, 5, 1, 3, 3, 3]

        clf = TabiMISO( orders=orders,
                        build_modals=[partial(
                            build_model,
                            video_model_path=video_model_path), ],
                        batch_size=50,
                        interactions=False,
                        epochs=200,
                        patience=20)

        clf.fit(X, y)

    """

    def __init__(self, orders, 
                build_fun=build_imiso,
                interactions=False,
                build_modals=[],
                pretrained_ws = None,
                *args, **kwargs):

        # Delete old graphs because the layer names may conflict
        tf.keras.backend.clear_session()
        super().__init__(build_fun=build_fun,
                         *args, **kwargs)
        self.orders = orders
        self.model_args['build_modals'] = build_modals
        self.n_modal = len(build_modals)
        self.interactions = interactions
        self.model_args['interactions'] = self.interactions
        self.pretrained_ws = pretrained_ws

    def _prepare_X(self, X):
        if self.n_modal > 0:
            # assume last list element is the tabular data to be exanded
            X_tab = X[-1]
            assert X_tab.shape[1] == len(
                self.orders), 'list of orders do not match tabular data dimensions {}'.format(len(X_tab))
            X2 = X[:self.n_modal] + expand(X_tab, self.orders)
        else:
            X2 = expand(X, self.orders)
        return X2

    def _prepare_fit_args(self, *args, **kwargs):
        super()._prepare_fit_args(*args,**kwargs)
        if not (self.pretrained_ws is None):
            self.loadINN(self.pretrained_ws)

    def fit(self, X, y=None, **kwargs):
        if isinstance(X, tf.keras.utils.Sequence):
            return super().fit(X, **kwargs)
        else:
            X2 = self._prepare_X(X)
            if 'validation_data' in kwargs.keys() and kwargs['validation_data']:
                X_val = self._prepare_X(kwargs['validation_data'][0])
                y_val = kwargs['validation_data'][1]
                kwargs['validation_data'] = (X_val, y_val)
            return super().fit(X2, y, **kwargs)

    def _predict(self, X, y=None, **kwargs):
        if isinstance(X, tf.keras.utils.Sequence):
            return super()._predict(X, **kwargs)
        else:
            X2 = self._prepare_X(X)
            return super()._predict(X2, y=y, **kwargs)


    @property
    def coef_(self):
        '''np.ndarray: Feature importance coefficients. Not including interaction terms.'''
        w_nn = self.model.get_layer('beta_polys').get_weights()[0]
        if 1 in self.orders:
            w_nc = self.model.get_layer('beta_linears').get_weights()[0]

        coefs = []
        nc, nn = (0, 0)
        for _ in range(self.n_modal):
            coefs.append(w_nn[nn])
            nn += 1

        for order in self.orders:
            if order == 1:
                coefs.append(w_nc[nc])
                nc += 1
            else:
                coefs.append(w_nn[nn])
                nn += 1

        return np.array(coefs).ravel()


    @property
    def gammas(self):
        '''np.ndarray: Interaction terms coefficients.'''
        int_type = type(self.interactions)
        self.gammas_ = []
        if self.interactions:
            w_in = self.model.get_layer('gammas').get_weights()[0]
            if int_type is bool:
                idx = 0
                for i in range(len(self.orders)):
                    for j in range(i + 1, len(self.orders)):
                        self.gammas_.append((w_in[idx][0], i, j))
                        idx += 1
            else:
                for n, iis in enumerate(self.interactions):
                    self.gammas_.append((w_in[n][0], iis[0], iis[1]))

        return self.gammas_

    @property
    def risk_funs(self):
        ''':obj:`list` of :obj:`Callable`: List of risk functions for each input. Only for tabular features.'''
        funs = []
        coefs = self.coef_[self.n_modal:]

        for no, order in enumerate(self.orders):
            if order == 1:
                def funcC(j):
                    def func(x):
                        if np.sign(j) >= 0:
                            return x
                        else:
                            return 1 - x
                    return func
                funs.append(funcC(coefs[no]))
            else:
                inp = self.model.input[no + self.n_modal]
                output = self.model.get_layer('poly_%d' % no).output
                model = Model(inputs=inp, outputs=output)

                def funcC(m, o):
                    def func(x):
                        return m.predict(expand_column(x, o))
                    return func
                funs.append(funcC(model, order))

        return funs

    @property
    def polys(self):
        ''':obj:`list` of :obj:`np.ndarray`: List of polynomial coefficients for each tabular input.'''

        polys = []
        coefs = self.coef_[self.n_modal:]
        for no, order in enumerate(self.orders):
            if order == 1:
                polys.append([coefs[no], 0])
            else:
                ws = self.model.get_layer('poly_%d' % no).get_weights()
                rev = list(reversed(list(map(float, ws[0]))))
                rev.append(float(ws[1]))
                polys.append(rev)
        return polys

    def saveINN(self, name='innws'):
        '''
        Function to save key layers for the INN : `beta_polys`, `beta_polys`, and `poly_#` 
        Note
        ----
        Use this function to send pre-trained weights without expensive modal training
        '''
        weights = {}

        weights['orders'] = self.orders
        weights['beta_polys'] = self.model.get_layer('beta_polys').get_weights()
        if 1 in self.orders:
            weights['beta_linears'] = self.model.get_layer('beta_linears').get_weights()
        if self.interactions:
            weights['gammas'] = self.model.get_layer('gammas').get_weights()
        for no, order in enumerate(self.orders):
            if order >1:
                weights['poly_%d' % no] = self.model.get_layer('poly_%d' % no).get_weights()
        
        with open(f'{name}.pkl', 'wb') as fid:
            pickle.dump(weights, fid)
        return weights


    def loadINN(self, name='innws'):
        '''
        Function to load key layers for the INN : `beta_polys`, `beta_polys`, and `poly_#` 
        Note
        ----
        Use this function to load pre-trained weights 
        '''
        with open(f'{name}.pkl', 'rb') as fid:
            weights = pickle.load(fid)
        assert weights['orders'] == self.orders, 'Orders do not match'

        if self.n_modal>0:
            mode_coefs = np.zeros((self.n_modal,1))
            weights['beta_polys'][0] = np.concatenate((mode_coefs, weights['beta_polys'][0]), axis=0)

        self.model.get_layer('beta_polys').set_weights(weights['beta_polys'])

        if 1 in self.orders:
            self.model.get_layer('beta_linears').set_weights(weights['beta_linears'])

        if self.interactions:
            self.model.get_layer('gammas').set_weights(weights['gammas'])

        for no, order in enumerate(self.orders):
            if order>1:
                self.model.get_layer(f'poly_{no}').set_weights(weights[f'poly_{no}'])
        return weights  

