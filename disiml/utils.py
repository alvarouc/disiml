import numpy as np
from skimage.transform import downscale_local_mean

def normalize_length(img, length):
    img_l = img.shape[0]
    if img_l < length:
        out = np.zeros([length] + list(img.shape[1:]))
        out[-img_l:, ...] = img
    elif img_l > length:
        out = img[:length, ...]
    else:
        out = img
    return out


def normalize_video(video, downsample, timesteps):

    if video.shape[0] >= timesteps:
        video = video[:timesteps, ...]

    if downsample > 1:

        if len(video.shape) == 3:
            factors = (downsample, downsample)
        elif len(video.shape) == 4:
            factors = (downsample, downsample, 1)

        video = [downscale_local_mean(
            frame, factors=factors) for frame in video]
        # with ThreadPool(2) as p:
        #     video = p.map(partial(downscale_local_mean,
        #                           factors=(downsample, downsample)), video)
        video = np.array(video)

    # video shape is #time, rows, cols
    if len(video.shape) == 3:
        video = np.expand_dims(normalize_length(video, timesteps), -1)
    else:
        video = normalize_length(video, timesteps)
        # video shape is #videos, time, rows, cols, rgb
    return video.astype('uint8')


