from tensorflow.python.keras.models import Model
from tensorflow.keras.layers import TimeDistributed
from tensorflow.keras.layers import (Input, BatchNormalization,
                                            Dense, ReLU, Dropout,
                                            # CuDNNLSTM,
                                            LSTM,
                                            Conv2D, Conv3D,
                                            ConvLSTM2D,
                                            MaxPooling2D, MaxPooling3D,
                                            Flatten, concatenate)
from tensorflow.python.keras import backend as K
from tensorflow.python.keras.backend import floatx
import numpy as np
import math
from tqdm import tqdm
from .predictor import SI, MI, SO


def normalize(x):
    # utility function to normalize a tensor by its L2 norm
    return x / (K.sqrt(K.mean(K.square(x))) + K.epsilon())


cnn_params = {
    'padding': 'same',
    'activation': None,
    # 'kernel_initializer': 'zeros',
}


def fc3d(x, n_layers, sk=3, tk=3, name=''):
    for layer in range(n_layers):
        # Conv3D->Norm->RELU->Conv3D->Norm->RELU->Pool
        nfilt = min(2**(layer + 2), 16)
        x = Conv3D(nfilt, (sk, sk, tk),
                   name='fc3d_conv3d_%d_%s' % (layer, name),
                   **cnn_params)(x)
        x = BatchNormalization(name='fc3d_bn_%d_%s' % (layer, name))(x)
        x = ReLU(name='fc3d_relu_%d_%s' % (layer, name))(x)
        x = Conv3D(nfilt, (sk, sk, tk),
                   name='fc3d_conv3d2_%d_%s' % (layer, name), **cnn_params)(x)
        x = BatchNormalization(name='fc3d_bn2_%d_%s' % (layer, name))(x)
        x = ReLU(name='fc3d_relu2_%d_%s' % (layer, name))(x)
        x = MaxPooling3D((3, 3, 3),
                name='fc3d_mp_%d_%s' % (layer, name))(x)

    return x

def build_siso(input_shape, loss, optimizer, metrics=None, num_class=1, **build_args):
    nsteps, nrows, ncols, _ = input_shape
    n_layers = math.floor(math.log(min(nrows, ncols, nsteps), 3))
    video_input = Input(shape=input_shape,
                        name='video_input')

    x = video_input
    x = fc3d(x, n_layers, name='3d2')
    x = Flatten(name='video_vector')(x)
    x = Dropout(.3, name='fc3d2_drop')(x)
    if num_class is None:
        output = Dense(1, name='fc3d2_out', activation='sigmoid')(x)
    else:
        output = Dense(num_class, activation='softmax')(x)
    model = Model(inputs=video_input, outputs=output)
    model.compile(loss=loss,optimizer=optimizer, metrics=metrics)

    return model


def build_miso(input_shape, loss, optimizer, metrics=None, num_class=1, aux_shape=None,
               lstm_units=128, *args, **kwargs):
    '''
    Builds a Keras model for prediction from video
    Inputs:
    - input_shape: (nframes, nrows, ncols, nchannels)
    - aux_shape: shape of auxiliary data (static EHR)
    - lstm_units: number of lstm_units
    Output:
    - model: keras model to fit and predict
    '''
    concat_layers = []
    main_inputs = []

    if len(input_shape[-1]) == 1:
        aux_shape = input_shape[-1]
        del input_shape[-1]
    else:
        aux_shape = None

    for ind, in_shape in enumerate(input_shape):
        video_input = Input(shape=in_shape,
                            dtype=floatx(),
                            name='main_input' + str(ind))
        main_inputs.append(video_input)

        x = TimeDistributed(Conv2D(32, (5, 5), strides=(2, 2),
                                   activation='relu',
                                   padding='valid'))(video_input)
        x = TimeDistributed(
            Conv2D(64, (3, 3), strides=(2, 2),
                   kernel_initializer="he_normal",
                   activation='relu', padding='valid'))(x)
        x = TimeDistributed(MaxPooling2D((3, 3), strides=(2, 2)))(x)
        x = TimeDistributed(Flatten())(x)
        video_out = LSTM(lstm_units, return_sequences=False, dropout=0.1)(x)

        concat_layers.append(video_out)

    if aux_shape is not None:  # concatenate aux data
        auxiliary_input = Input(shape=aux_shape, name='aux_input')
        main_inputs.append(auxiliary_input)
        aux_out = Dense(32, activation='sigmoid')(auxiliary_input)
        concat_layers.append(aux_out)

    x = concatenate(concat_layers, name='concat_output_aux')
    x = Dense(32, activation='sigmoid')(x)

    if num_class > 2:
        main_output = Dense(
            num_class, activation='softmax', name='main_output')(x)
    else:
        main_output = Dense(1, activation='sigmoid', name='main_output')(x)

    model = Model(inputs=main_inputs,
                  outputs=main_output)
    model.compile(loss=loss,optimizer=optimizer, metrics=metrics)

    return model


class VideoSISO(SI, SO):
    '''
    Class that implements a video predictor model
    that is sklearn compatible
    '''

    def __init__(self, build_fun=build_siso,
                 *args, **kwargs):
        super().__init__(build_fun=build_fun,
                         *args, **kwargs)


    def max_activation(self, layer_name='fc3d_relu2_2_', filter_idx=-1):
        '''Computes the image that best activates a given filter (filter_idx)
        and layer (layer_name)

        Inputs
        - layer_name: name of the layer with CNNs
        - filter_idx: filter number to activate
        Output
        - list of video responses
        '''

        # dimensions of the generated pictures for each filter.
        n_frames, img_width, img_height, n_channels = self.model_args['input_shape']

        # this is the placeholder for the input images
        input_img = self.model.input
        # get the symbolic outputs of each "key" layer (we gave them unique names).
        layer_dict = dict([(layer.name, layer)
                           for layer in self.model.layers[1:]])
        imgs = []

        n_filters = layer_dict[layer_name].get_config()['filters']
        for filter_index in range(n_filters):
            # we only scan through the first 32 filters,
            print('Processing filter %d' % filter_index)
            # we build a loss function that maximizes the activation
            # of the nth filter of the layer considered
            layer_output = layer_dict[layer_name].output
            loss = K.mean(layer_output[..., filter_index])

            # we compute the gradient of the input picture wrt this loss
            grads = K.gradients(loss, input_img)[0]

            # normalization trick: we normalize the gradient
            grads = normalize(grads)

            # this function returns the loss and grads given the input picture
            iterate = K.function([input_img], [loss, grads])

            # step size for gradient ascent
            step = 1.

            input_img_data = np.random.random(
                (1, n_frames, img_width, img_height, 1))
            input_img_data = (input_img_data - 0.5) * 20 + 128

            # we run gradient ascent for 20 steps
            for i in tqdm(range(100)):
                loss_value, grads_value = iterate([input_img_data])
                input_img_data += grads_value * step

                if loss_value <= 0.:
                    # some filters get stuck to 0, we can skip them
                    break

            print('Current loss value:', loss_value)

            # decode the resulting input image
            if loss_value > 0:
                imgs.append(input_img_data[0])
            else:
                imgs.append(-1)

        return imgs


class VideoMISO(MI, SO):

    def __init__(self, build_fun=build_miso, *args, **kwargs):
        super().__init__(build_fun=build_fun, *args, **kwargs)

  
