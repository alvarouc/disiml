Auto Encoders
=======================
Use to get reduced dimensionality of data. 

.. autoclass:: disiml.ae.BaseEncoder

.. autoclass:: disiml.ae.AutoEncoder

.. autoclass:: disiml.ae.SeriesAE

.. toctree::
   :maxdepth: 2
   :caption: Contents: