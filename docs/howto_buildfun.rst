How to create a custom build function
=====================================

DISIML classifier objects support custom keras model build functions. 

A minimal build function should look like the following::

    from tensorflow.python.keras.models import Model
    from tensorflow.python.keras.layers import (Input, Dense)

    def build_siso(input_shape, loss, optimizer, metrics):

        a_input = Input(shape=input_shape, name='tabular_input')
        x = Dense(100, activation='sigmoid')(a_input)
        x = Dense(10, activation='sigmoid')(x)
        output = Dense(1, activation='sigmoid')(x)
        model = Model(inputs=a_input, outputs=output)
        model.compile(loss=loss,optimizer=optimizer, metrics=metrics)

        return model


Since DISIML classifier objects infer the data shape when fit is called, we call the build function internally. DISIML get the input shapes from the numpy array or data generator provided. 

Then we can pass our custom build function to any classifier object as::

    from disiml import TabSISO
    import numpy as np
    X = np.random.random((1000,10))
    y = np.random.uniform(low=0, high=1, size=(1000, 1)) > .5

    clf = TabSISO(build_fun=build_siso)
    clf.fit(X,y) # <--Here build_siso is called with the shape of X
    y_pred = clf.predict(X)

Additional Parameters
---------------------
    In case the build function requires parameters not defined by :ref:`base-class`, we can provide custom parameter through the `build_args` input parameter. 

    For example::

        from disiml import TabSISO
        import numpy as np
        from tensorflow.python.keras.models import Model
        from tensorflow.python.keras.layers import (Input, Dense)

        def build_siso(input_shape, loss, optimizer, metrics, layer_name):

            a_input = Input(shape=input_shape, name='tabular_input')
            x = Dense(100, activation='sigmoid')(a_input)
            x = Dense(10, activation='sigmoid', name=layer_name)(x)
            output = Dense(1, activation='sigmoid')(x)
            model = Model(inputs=a_input, outputs=output)
            model.compile(loss=loss,optimizer=optimizer, metrics=metrics)

            return model

        X = np.random.random((1000,10))
        y = np.random.uniform(low=0, high=1, size=(1000, 1)) > .5

        clf = TabSISO(
            build_fun=build_siso, 
            build_args={'layer_name': 'second_layer'})
        clf.fit(X,y) 
        y_pred = clf.predict(X)

    Internally, the build_fun is called as::
    
        self.model = self.build_fun(**self.model_args) 
        
    where::
        
        self.model_args = dict(
            verbose=self.verbose, 
            loss = self.loss,
            optimizer=self.optimizer,
            metrics=self.metrics,
            **build_args)
    