
DISIML: Documentation
=====================
.. image:: https://gitlab.com/alvarouc/disiml/badges/master/pipeline.svg 
.. image:: https://gitlab.com/alvarouc/disiml/badges/master/coverage.svg
   :target: https://alvarouc.gitlab.io/disiml/cov

A keras wrapper for scikit-learn support. All classification classes support 

- fit 
- transform
- predict
- predict_proba

as defined in the scikit-learn standard.

.. toctree::
   :maxdepth: 2
   :caption: Classification:

   predictor
   tabular
   series
   video
   howto_buildfun

.. toctree::
   :maxdepth: 2
   :caption: Data Ingestion:

   generator

.. toctree::
   :maxdepth: 2
   :caption: Unsupervised:

   encoder


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
