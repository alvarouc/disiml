Time Series Classifiers
=======================
Use to predict binary labels from time-series data. 

.. note:: See :ref:`base-class` for a full description of input parameters.

Single Input - Single Output 
----------------------------
.. autoclass:: disiml.series.SeriesSISO

Multiple Input - Single Output
------------------------------
.. autoclass:: disiml.series.SeriesMISO

Single Input - Multiple Output
------------------------------
.. autoclass:: disiml.series.SeriesSIMO

.. toctree::
   :maxdepth: 2
   :caption: Contents:
