Tabular Classifiers
=======================
Use to predict binary labels from tabular data. 

.. note:: See :ref:`base-class` for a full description of input parameters.

Interpretable Neural Network: Multiple Input - Single Output
------------------------------------------------------------

.. autoclass:: disiml.tabular.INNGenerator
.. autoclass:: disiml.tabular.TabiMISO


Single Input - Single Output 
----------------------------
.. autoclass:: disiml.tabular.TabSISO



.. toctree::
   :maxdepth: 2
   :caption: Contents: