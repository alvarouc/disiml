Video Classifiers
=======================
Use to predict binary labels from video data. 

.. note:: See :ref:`base-class` for a full description of input parameters.

Single Input - Single Output 
----------------------------
.. autoclass:: disiml.video.VideoSISO

Multiple Input - Single Output
------------------------------
.. autoclass:: disiml.video.VideoMISO

.. toctree::
   :maxdepth: 2
   :caption: Contents: