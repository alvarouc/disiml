from distutils.core import setup

setup(name='disiml',
      version='1.1',
      description='Keras models for EHR data',
      author='Alvaro Ulloa',
      author_email='aeulloacerna@geisinger.edu',
      packages=['disiml', ],
      )
