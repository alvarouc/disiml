import pytest
from sklearn.datasets import make_blobs
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error as mse
import numpy as np
import sys
import os
from sklearn.preprocessing import StandardScaler
sys.path.insert(0, os.path.abspath(
    os.path.join(os.path.dirname(__file__), '../')))

from disiml import AutoEncoder

X, y = make_blobs(
    n_samples=1000, 
    n_features=5,
    centers=3,
    center_box=(-20, 20),
    random_state=1988)


def test_sk_ae():
    sk = AutoEncoder(layers_dim=[100, 100, 5], epochs=200, scaler =StandardScaler())

    X2 = sk.fit_transform(X)

    Xr = sk.inverse_transform(X2)
    error = mse(X, Xr)
    


def test_ae_scaler():
    sk = AutoEncoder(
        scaler=StandardScaler(), 
        layers_dim=[20, 2])
