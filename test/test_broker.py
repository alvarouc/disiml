import pytest
import sys
import os
import h5py
import numpy as np
import pandas as pd
from shutil import rmtree

sys.path.insert(0, os.path.abspath(
    os.path.join(os.path.dirname(__file__), '../')))
from disiml import (HDFBroker, HDFBrokers,
                    FileBroker, FileBrokers,
                    FileGenerator)  # noqa: E402

X = np.ones((10, 10, 10), dtype='int8')
AUX = pd.DataFrame(np.ones((10, 3)), index=[str(x) for x in range(10)])
SIZE = 10 * X.nbytes / 1e9

def setup_module(module):
    """ setup any state specific to the execution of the given module."""
    # Create HDF5 files
    with h5py.File('deleteme.hdf5', 'w') as f:
        for n in range(20):
            f.create_dataset(str(n), data=X * n, dtype='i8')

    with h5py.File('deleteme2.hdf5', 'w') as f:
        for n in range(20):
            f.create_dataset(str(n), data=X * n, dtype='i8')

    # Create folder with numpy arrays
    folders = ['deleteme_a', 'deleteme_b']
    for n, folder in enumerate(folders):
        os.makedirs(folder)
        tmp = (n+1) * np.arange(100*(n+1)**2).reshape((10*(n+1), 10*(n+1)))
        for i in range(20):
            np.save(f'{folder}/{i}.npy', tmp)


def teardown_module(module):
    """teardown any state that was previously setup with a setup_module
    method.
    """

    os.remove('deleteme.hdf5')
    os.remove('deleteme2.hdf5')
    rmtree('deleteme_a')
    rmtree('deleteme_b')


def test_HDFBroker():

    broker = HDFBroker(data_path='deleteme.hdf5',
                       cache_size=4,
                       cache_folder='HDFdeleteme')
    r = broker.read([str(x) for x in range(20)])
    assert r.shape == (20, 10, 10, 10)
    for n in range(20):
        assert r[n, 0, 0, 0] == n


def test_HDFBrokers():
    broker = HDFBrokers(data_paths=['deleteme.hdf5', 'deleteme2.hdf5'],
                        preprocess_funs=[lambda x:x, lambda x:x],
                        cache_size=4)
    data = broker.read([str(x) for x in range(5)])
    assert data[0].shape == (5, 10, 10, 10)
    assert data[1].shape == (5, 10, 10, 10)
    for n in range(5):
        assert data[0][n, 0, 0, 0] == n


def test_FileBroker():

    broker = FileBroker(data_path='deleteme_a', 
                        cache_size=4)
    r = broker.read([str(i) for i in range(20)])
    assert r.shape == (20, 10, 10)
    tmp = np.arange(100).reshape((10, 10))
    for n in range(20):
        np.testing.assert_equal(r[n], tmp)

def test_FileBroker_w_aux():
    names = [str(i) for i in range(20)]
    df = pd.DataFrame({key: range(10) for key in names}).T
    broker = FileBroker(data_path='deleteme_a', aux= df, 
                        cache_size=4)
    r = broker.read(names)
    assert r[0].shape == (20, 10, 10)
    tmp = np.arange(100).reshape((10, 10))
    for n in range(20):
        np.testing.assert_equal(r[0][n], tmp)

    assert r[1].shape == (20,10)

def test_FileBrokers():
    folders = ['deleteme_a', 'deleteme_b']
    broker = FileBrokers(data_paths=folders,
                         #preprocess_funs=[lambda x:x, lambda x:x],
                        cache_size=4)
    r = broker.read([str(x) for x in range(20)])
    assert len(r) == 2
    assert r[0].shape == (20, 10, 10)
    assert r[1].shape == (20, 20, 20)

    for n in range(len(folders)):
        tmp = (n+1) * np.arange(100*(n+1)**2).reshape((10*(n+1), 10*(n+1)))
        for i in range(20):
            np.testing.assert_equal(r[n][i], tmp)


def test_broker_no_ram():
    broker = HDFBroker(data_path='deleteme.hdf5', cache_size=1)
    r = broker.read([str(x) for x in range(5)], pbar=True)
    assert r.shape == (5, 10, 10, 10)
    for n in range(5):
        assert r[n, 0, 0, 0] == n


def test_broker_index_types():
    broker = HDFBroker(data_path='deleteme.hdf5', cache_size=1)
    r = broker.read(tuple([str(x) for x in range(5)]))
    assert r.shape == (5, 10, 10, 10)
    for n in range(5):
        assert r[n, 0, 0, 0] == n

    r = broker.read('0')
    assert r.shape == (1, 10, 10, 10)
    assert r[0, 0, 0, 0] == 0


def test_broker_save_load():
    broker = HDFBroker(data_path='deleteme.hdf5', cache_size=1)
    r = broker.read(tuple([str(x) for x in range(5)]))
    broker.save()
    assert r.shape == (5, 10, 10, 10)
    for n in range(5):
        assert r[n, 0, 0, 0] == n

    broker2 = HDFBroker(data_path='deleteme.hdf5', cache_size=1)
    broker2.load('./')
    s = broker.read(tuple([str(x) for x in range(5)]))
    assert s.shape == (5, 10, 10, 10)
    for n in range(5):
        assert s[n, 0, 0, 0] == n


def test_auxbroker():
    broker = HDFBroker(data_path='deleteme.hdf5', cache_size=1,
                       aux=AUX)
    r = broker.read([str(x) for x in range(5)])
    assert r[0].shape == (5, 10, 10, 10)
    assert r[1].shape == (5, 3)

    for n in range(5):
        assert r[0][n, 0, 0, 0] == n


def test_series_generator():

    y_train = pd.Series([True if tt%2 else False for tt in range(20)])
    generator = FileGenerator(data_path='deleteme_a', y_set=y_train, batch_size = 5)
    Xg, yg= generator[0]
    assert Xg.shape == (5, 10, 10)
    assert yg.shape == (5,)


def test_series_generator_waux():

    y_train = pd.Series([True if tt%2 else False for tt in range(20)])
    aux = pd.DataFrame(np.random.random((len(y_train), 7)))

    generator = FileGenerator(
        data_path='deleteme_a', 
        y_set=y_train, 
        batch_size = 5,
        aux=aux)
    Xg, yg= generator[0]

    assert len(Xg)==2
    assert Xg[0].shape == (5, 10, 10)
    assert Xg[1].shape == (5, 7)
    assert yg.shape == (5,)