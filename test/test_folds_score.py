import pytest
import pandas as pd
from sklearn.datasets import make_classification
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier as RF
from sklearn.linear_model import LogisticRegression as LR
from sklearn.decomposition import PCA
from sklearn.pipeline import Pipeline
from sklearn.feature_selection import SelectKBest
import sys
import os
import numpy as np
sys.path.insert(0, os.path.abspath(
    os.path.join(os.path.dirname(__file__), '../')))  # noqa: E402
from disiml.cv import generate_folds, score


NSAMPLES = 1000
NFOLDS = 2
X, y = make_classification(n_samples=NSAMPLES, n_features=50,
                           n_informative=10, n_redundant=40,
                           n_repeated=0, n_classes=2,
                           n_clusters_per_class=1, weights=None,
                           flip_y=0.01, class_sep=2.0,
                           hypercube=True, shift=0.0,
                           scale=1.0, shuffle=True,
                           random_state=1988)


def test_folds():

    df = pd.DataFrame.from_dict({'PT_MRN':
                                 [0, 1, 1, 1, 2, 3, 3, 4, 4, 4,
                                  5, 6, 7, 8, 9],
                                 'PT_STATUS':
                                 ['DECEASED', ] * 7 + ['ALIVE'] * 3 +
                                 ['DECEASED', ] * 5,
                                 'TODAY_DT':
                                 [0, 0, 1, 2, 0, 0, 1, 0, 1, 2,
                                  0, 0, 0, 0, 0],
                                 'PT_SURVIVAL':
                                 [10, 20, 30, 40, 50, 60, 70, 50,
                                  20, 10, 10, 20, 30, 40, 50],
                                 })
    df.index = [chr(ord('a')+n) for n in range(15)]

    kf, dead = generate_folds(df, n_folds=NFOLDS, th=30, by='first', seed=1988)
    assert len(kf) == NFOLDS, 'wrong number of folds'
    assert len(dead) == df.shape[0], 'label is not same dimention as data'


def test_folds_w_val():

    df = pd.DataFrame.from_dict({'PT_MRN':
                                 [0, 1, 1, 1, 2, 3, 3, 4, 4, 4,
                                  5, 6, 7, 8, 9],
                                 'PT_STATUS':
                                 ['DECEASED', ] * 7 + ['ALIVE'] * 3 +
                                 ['DECEASED', ] * 5,
                                 'TODAY_DT':
                                 [0, 0, 1, 2, 0, 0, 1, 0, 1, 2,
                                  0, 0, 0, 0, 0],
                                 'PT_SURVIVAL':
                                 [1, 2, 3, 4, 5, 6, 7, 5, 2, 1,
                                  1, 2, 3, 4, 5],
                                 })

    kf, dead = generate_folds(df, n_folds=NFOLDS, th=3,
                              by='first', seed=1988, validation_split=0.1)
    assert len(kf) == NFOLDS, 'wrong number of folds'
    assert len(dead) == df.shape[0], 'label is not same dimention as data'

    for k, (train, val, test) in enumerate(kf):

        assert len(train) > 0, 'No training generated'
        train_mrns = set(df.iloc[train]['PT_MRN'])
        test_mrns = set(df.iloc[test]['PT_MRN'])

        # Check wheter the same PT_MRN is in train and test
        assert not (train_mrns & test_mrns),\
            'Fold {}: Train and test folds have repeated MRNs'.format(k)
        # Check wheter the invalid studies were added
        assert 8 not in train and 9 not in train
        assert 8 not in test and 9 not in test


def test_score_without_grid():

    pipe = Pipeline([('scaler', StandardScaler()),
                     ('features', PCA(n_components=10)),
                     ('classifier', RF())])

    skf = StratifiedKFold(n_splits=NFOLDS, random_state=1988)
    kfold = list(skf.split(X, y))

    results = score(pipe, {}, X, y, kfold, n_jobs=1)
    aucs = results['AUCs']
    fimps = results['FIMPs']

    assert len(aucs) == NFOLDS, 'Returned {} instead of {}'.format(
        len(aucs), NFOLDS)
    assert all([auc > 0.9 for auc in aucs])
    assert len(fimps) == NFOLDS, 'Returned {} instead of {}'.format(
        len(fimps), NFOLDS)


def test_score_without_grid_coef():
    pipe = Pipeline([('scaler', StandardScaler()),
                     ('features', PCA(n_components=10)),
                     ('classifier', LR())])

    skf = StratifiedKFold(n_splits=NFOLDS, random_state=1988)
    kfold = list(skf.split(X, y))

    results = score(pipe, {}, X, y, kfold, n_jobs=1, y_weights=np.ones_like(y))
    aucs = results['AUCs']
    fimps = results['FIMPs']

    assert len(aucs) == NFOLDS, 'Returned {} instead of {}'.format(
        len(aucs), NFOLDS)
    assert all([auc > 0.9 for auc in aucs])
    assert len(fimps) == NFOLDS, 'Returned {} instead of {}'.format(
        len(fimps), NFOLDS)


def test_score_with_grid():

    pipe = Pipeline([('scaler', StandardScaler()),
                     ('features', SelectKBest()),
                     ('classifier', RF())])

    skf = StratifiedKFold(n_splits=NFOLDS, random_state=1988)
    kfold = list(skf.split(X, y))
    results = score(pipe, {'features__k': [2, 10, 20],
                           'scaler__with_std': [True, False],
                           'classifier__n_estimators': [10, 20]},
                    X, y, kfold, n_jobs=1)
    aucs = results['AUCs']
    fimps = results['FIMPs']

    assert len(aucs) == NFOLDS, 'Returned {} instead of {}'.format(
        len(aucs), NFOLDS)
    assert all([auc > 0.9 for auc in aucs])
    assert len(fimps) == NFOLDS, 'Returned {} instead of {}'.format(
        len(fimps), NFOLDS)


def test_score_custom():

    pipe = Pipeline([('scaler', StandardScaler()),
                     ('features', SelectKBest(k=10)),
                     ('classifier', RF())])

    skf = StratifiedKFold(n_splits=NFOLDS, random_state=1988)
    kfold = list(skf.split(X, y))
    results = score(pipe, {'scaler__with_std': [True, False],
                           'classifier__n_estimators': [10, 20]},
                    X, y, kfold, n_jobs=1,
                    custom=lambda x: x.n_features_)

    assert results['Custom'] == [10, 10]
