import pytest
import numpy as np
from sklearn.metrics import roc_auc_score
import sys
import os
sys.path.insert(0, os.path.abspath(
    os.path.join(os.path.dirname(__file__), '../')))
from disiml import TabSISO

NSAMPLES = 500
NCOLS = 10

X = np.concatenate(
    (np.ones((NSAMPLES // 2, NCOLS)),
     np.ones((NSAMPLES // 2, NCOLS)) * 128),
    axis=0)


y = np.array(([0, ] * (NSAMPLES // 2)) + ([1] * (NSAMPLES // 2)))


def test_save_tabsiso():

    clf = TabSISO(epochs=10)
    clf.fit(X, y)
    y_pred = clf.predict_proba(X)

    clf.save('test_model')

    clf2 = TabSISO()
    clf2.load('test_model')

    assert clf2.model.get_config() == clf.model.get_config()
    assert all([(w1 == w2).all() for w1, w2 in zip(
        clf2.model.get_weights(), clf.model.get_weights())])

    y1 = clf.predict(X)
    y2 = clf2.predict(X)

    assert (y1 == y2).all()


def test_checkpoint():

    clf = TabSISO(epochs=10, checkpoint=2)
    clf.fit(X, y)
    y_pred = clf.predict_proba(X)
