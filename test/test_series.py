import pytest
import numpy as np
import pandas as pd
import sys
import os
from sklearn.preprocessing import OneHotEncoder
import gc
import time
from sklearn import metrics
from sklearn.utils import class_weight
from sklearn.utils.class_weight import compute_sample_weight
import tensorflow as tf
from shutil import rmtree

sys.path.insert(0, os.path.abspath(
    os.path.join(os.path.dirname(__file__), '../')))  # noqa: E402
from disiml import (
    SeriesSISO, SeriesMISO, SeriesSIMO, SeriesMIMO, 
    SeriesAE, FileBroker, FileGenerator)  # noqa: E402


# Prepare dummy data
NSAMPLES = 100
NSTEPS = 20
NSERIES = 2
NAUXFEATURES = 2

enc = OneHotEncoder(handle_unknown='ignore', sparse=False)
X = np.ones((NSAMPLES, NSTEPS, NSERIES))
X1 = X
ymulti = np.array([t%4 for t in range(NSAMPLES)]).reshape(-1,1)
y = (ymulti==0) | (ymulti==1)
X[ymulti.ravel()==0]+=10
X[ymulti.ravel()==1]+=20
X[ymulti.ravel()==2]+=30
X[ymulti.ravel()==3]+=40
ymulti = enc.fit_transform(ymulti)
aux = pd.DataFrame(np.ones((NSAMPLES, NAUXFEATURES)))
aux[y.ravel()]+=10
VAL_NSAMPLES = int(NSAMPLES * 0.2)
val_X = X[:VAL_NSAMPLES]
val_X1 = X1[:VAL_NSAMPLES]
val_y = y[:VAL_NSAMPLES]
val_ymulti = ymulti[:VAL_NSAMPLES]
val_aux = aux.iloc[:VAL_NSAMPLES]

 
def test_series_siso():

    clf = SeriesSISO(epochs=2)
    clf.fit(X, y)
    y_pred = clf.predict_proba(X)
    assert y_pred.shape == (X.shape[0], 1)


def test_series_generator():

    # Create folder with numpy arrays

    os.makedirs('deleteme', exist_ok=True)
    for i in range(NSAMPLES):
        np.save(f'deleteme/{i}.npy', X[i])

    train = list(range(NSAMPLES//2))
    val = list(range(NSAMPLES//2,NSAMPLES))

    yp = pd.Series(y.ravel())
    # Test generator
    train_generator = FileGenerator(data_path='deleteme', y_set=yp.loc[train], batch_size = 10)
    clf = SeriesSISO(epochs=2)
    clf.fit(train_generator)
    ypred = clf.predict(train_generator)

    clf = SeriesSISO(epochs=2)
    clf.fit(train_generator)
    ypred = clf.predict(train_generator)

    # Test multiclass
    y_df = pd.DataFrame(np.concatenate([yp[:,None], 1-yp[:, None]], axis =1))
    train_generator = FileGenerator(data_path='deleteme', y_set=y_df.loc[train], batch_size = 10)
    clf = SeriesSISO(epochs=2, loss="categorical_crossentropy")
    clf.fit(train_generator)

    rmtree('deleteme')

def test_series_siso_w_val():
    clf = SeriesSISO(epochs=2, batch_size=5)
    clf.fit(X, y, validation_data=[val_X, val_y])
    y_pred = clf.predict_proba(X)
    assert y_pred.shape == (X.shape[0], 1)


def test_series_siso_w_val_w_sw():
    clf = SeriesSISO(epochs=2)
    clf.fit(X, y, validation_data=[val_X, val_y, compute_sample_weight("balanced", val_y)])
    y_pred = clf.predict_proba(X)
    assert y_pred.shape == (X.shape[0], 1)


def test_series_siso_w_val_w_ymulti():

    clf = SeriesSISO(epochs=2)
    clf.fit(X, ymulti, validation_data=[val_X, val_ymulti])
    y_pred = clf.predict_proba(X)
    assert y_pred.shape == ymulti.shape


def test_series_miso_w_aux():

    clf = SeriesMISO(epochs=2)
    clf.fit([X, aux], y)
    y_pred = clf.predict_proba([X, aux])
    assert y_pred.shape == (X.shape[0], 1)


def test_series_miso_w_aux_w_val_data():

    clf = SeriesMISO(epochs=2)
    clf.fit([X, aux], y, validation_data=[[val_X, val_aux], val_y])
    y_pred = clf.predict_proba([X, aux])
    assert y_pred.shape == (X.shape[0], 1)


def test_series_miso_w_val():
    clf = SeriesMISO(epochs=2)
    clf.fit([X, X1], y, validation_data=[[val_X, val_X1], val_y])
    y_pred = clf.predict_proba([X, X1])
    assert y_pred.shape == (X.shape[0], 1)


def test_series_miso_ymulti():
    clf = SeriesMISO(epochs=2)
    clf.fit([X, X1], ymulti)
    y_pred = clf.predict_proba([X, X1])
    assert y_pred.shape == ymulti.shape


def test_series_simo_w_val():
    clf = SeriesSIMO(epochs=2)
    clf.fit(X, [y, ymulti], validation_data=[
            val_X, [val_y, val_ymulti]], weight_classes=True)
    y_pred = clf.predict_proba(X)
    assert len(y_pred) == len([y, ymulti])


def test_series_mimo():
    clf = SeriesMIMO(epochs=2)
    clf.fit([X, X1], [y, ymulti],  validation_data=[
            [val_X, val_X1], [val_y, val_ymulti]])
    y_pred = clf.predict_proba([X, X1])
    assert len(y_pred) == len([y, ymulti])


def test_series_mimo_with_aux():
    clf = SeriesMIMO(epochs=2)
    clf.fit([X, aux], [y, ymulti], validation_data=[
            [val_X, val_aux], [val_y, val_ymulti]])
    y_pred = clf.predict_proba([X, aux])
    assert len(y_pred) == len([y, ymulti])


def test_multiple_label_series_siso():
    clf = SeriesSISO(epochs=2)
    clf.fit(X, ymulti)
    y_pred = clf.predict_proba(X)
    assert y_pred.shape == ymulti.shape


def test_multiple_input_no_aux_series_miso():
    clf = SeriesMISO(epochs=2)
    clf.fit([X, X1], y)
    y_pred = clf.predict_proba([X, X1])
    assert y_pred.shape == (X.shape[0], 1)


def test_multiple_input_w_aux_series_predictor():
    clf = SeriesMISO(epochs=2)
    clf.fit([X, X1, aux], y)
    y_pred = clf.predict_proba([X, X1, aux])
    assert y_pred.shape == (X.shape[0], 1)


def test_lstm_ouput():

    clf = SeriesSISO(epochs=2)
    clf.fit(X, y)
    X2 = clf.transform(X, 'encoded')
    assert len(X2.shape) == len(X.shape)


def test_lstm_ouput_mi():

    clf = SeriesMISO(epochs=2)
    clf.fit([X, X1], y)
    X2 = clf.transform([X, X1], 'concat_output')
    assert X2.shape == (X.shape[0], 2 * clf.lstm_units)


def test_lstm_ouput_w_aux():

    clf = SeriesMISO(epochs=2)
    clf.fit([X, aux], y)
    X2 = clf.transform([X, aux], 'concat_output')
    # 32 is the default nodes in Dense for aux
    assert X2.shape == (X.shape[0], clf.lstm_units + 32)


def test_build_fun_as_argument():

    from tensorflow.python.keras import Model
    from tensorflow.python.keras.layers import Dense, LSTM, Input

    def build_ecg_model(*args, **kwargs):
        main_input = Input(shape=(NSTEPS, NSERIES))
        lstm_out = LSTM(4)(main_input)
        main_output = Dense(1, activation='sigmoid')(lstm_out)
        model = Model(inputs=main_input, outputs=main_output)
        model.compile(optimizer='Adam', loss='binary_crossentropy')
        return model

    clf = SeriesSISO(epochs=2, build_fun=build_ecg_model)
    clf.fit(X, y)
    y_pred = clf.predict_proba(X)
    assert y_pred.shape == (X.shape[0], 1)


def test_default_optimizer():

    clf = SeriesSISO(epochs=2)
    clf.fit(X, y)
    assert 'Adam' in str(clf.model.optimizer)

def test_adam_optimizer():

    clf = SeriesSISO(epochs=2, optimizer='SGD')
    clf.fit(X, y)
    assert 'SGD' in str(clf.model.optimizer)

def test_learning_rate():
    from tensorflow.python.keras import backend as K
    # test default
    clf = SeriesSISO(epochs=2)
    assert clf.learning_rate is None
    clf.fit(X, y)
    assert np.isclose(float(K.eval(clf.model.optimizer.lr)), 0.001)

    # test custom
    clf = SeriesSISO(epochs=2, learning_rate=0.1)
    assert clf.learning_rate == 0.1
    clf.fit(X, y)
    assert np.isclose(float(K.eval(clf.model.optimizer.lr)), 0.1)

    # test sgd
    clf = SeriesSISO(epochs=2, learning_rate=0.1, optimizer='SGD')
    assert clf.learning_rate == 0.1
    clf.fit(X, y)
    assert np.isclose(float(K.eval(clf.model.optimizer.lr)), 0.1)


def test_custom_callback_basic():
    clf = SeriesSISO(epochs=2, callbacks=[tf.keras.callbacks.ReduceLROnPlateau
(monitor='acc')])
    clf.fit(X, y)
    assert vars(clf.fit_args['callbacks'][0])['monitor'] == 'acc'


def test_custom_callback_val_acc():
    from tensorflow.python.keras import backend as K
    clf = SeriesSISO(epochs=2, callbacks=[tf.keras.callbacks.ReduceLROnPlateau
(monitor='val_acc')])
    clf.fit(X, ymulti, validation_data=[val_X, val_ymulti])
    idx = np.where(
        [cb.__class__ is tf.keras.callbacks.ReduceLROnPlateau
 for cb in clf.fit_args['callbacks']])[0][0]
    assert vars(clf.fit_args['callbacks'][idx])['monitor'] == 'val_acc'


def test_series_encoder():
    ae = SeriesAE(epochs=2)
    ae.fit(X)
    Xe = ae.transform(X) # get encoding units
    assert Xe.shape[0] == X.shape[0]

def test_xfer_series():

    # fit unlabel data
    ae = SeriesAE(epochs=2, verbose=True)
    ae.fit(X)
    # get encoder weights
    from tensorflow.python.keras.layers import Dense, Dropout, Flatten
    from tensorflow.python.keras.models import Model
    def build_1dcnn_clf(input_shape, loss, optimizer, metrics=None, encoder=ae.encoder, *args, **kwargs):
        input_node = encoder.input
        x = Flatten()(encoder.output)
        x = Dense(32, activation='sigmoid')(x)
        x = Dropout(0.5)(x)
        output_node = Dense(1, activation='sigmoid', name='main_output')(x)
        model = Model(inputs=input_node, outputs=output_node)
        model.compile(loss=loss,optimizer=optimizer, metrics=metrics)
        return model
    clf = SeriesSISO(build_fun=build_1dcnn_clf, epochs=2)
    clf.fit(X, y)
    assert np.isclose(ae.encoder.get_weights()[0], clf.model.get_weights()[0]).all()
    y_pred = clf.predict(X)
