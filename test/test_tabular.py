from tensorflow.keras.layers import (Input, BatchNormalization,
                                            Dense, Conv3D,
                                            MaxPooling3D,
                                            Flatten, concatenate)
from tensorflow.keras.regularizers import l1
from tensorflow.keras.constraints import NonNeg
from tensorflow.keras.models import Model
import pytest
import numpy as np
import pandas as pd
from sklearn.metrics import roc_auc_score
import sys
import os
from shutil import rmtree

sys.path.insert(0, os.path.abspath(
    os.path.join(os.path.dirname(__file__), '../')))

from disiml import TabSISO, TabiMISO, INNGenerator  # noqa

NSAMPLES = 500
NCOLS = 10

X = np.zeros((NSAMPLES, NCOLS))
y = np.array([v % 2 for v in range(NSAMPLES)])
X[y==1] += 1

X_val = np.zeros((NSAMPLES//10, NCOLS))
y_val = np.array([v % 2 for v in range(NSAMPLES//10)])
X_val[y_val==1] += 1

def test_tabsiso():

    clf = TabSISO(epochs=2)
    clf.fit(X, y)
    y_pred = clf.predict_proba(X)


def test_tabsiso_multiclass():
    y_mc = np.concatenate([y[:,None], 1-y[:, None]], axis =1)
    clf = TabSISO(epochs=2, loss="categorical_crossentropy")
    clf.fit(X, y_mc)

def test_tabsiso():

    clf = TabSISO(epochs=2, checkpoint=1)
    clf.fit(X, y)
    y_pred = clf.predict_proba(X)

def test_tabsiso_validation():

    clf = TabSISO(epochs=2)
    clf.fit(X, y, validation_data=[X_val, y_val])
    y_pred = clf.predict_proba(X)

def test_tabimiso():

    clf = TabiMISO(orders=[3 for _ in range(NCOLS)], epochs=2)
    clf.fit(X, y)
    y_pred = clf.predict_proba(X)
    assert len(clf.coef_) == X.shape[1]
    assert all(clf.coef_ >= 0)

    clf = TabiMISO(orders=[3 for _ in range(NCOLS)],
                   epochs=2, interactions=True)
    clf.fit(X, y)
    y_pred = clf.predict_proba(X)
    assert len(clf.coef_) == X.shape[1]
    assert all(clf.coef_ >= 0)
    assert len(clf.gammas) == 45


    clf = TabiMISO(orders=[3 for _ in range(NCOLS)],
                   epochs=2, interactions=[(0,9), (1,2),])
    clf.fit(X, y)
    y_pred = clf.predict_proba(X)
    assert len(clf.coef_) == X.shape[1]
    assert all(clf.coef_ >= 0)
    assert len(clf.gammas) == 2


def test_tabimiso_w_val():
    clf = TabiMISO(orders=[3 for _ in range(NCOLS)],
                   epochs=2, interactions=True)
    clf.fit(X, y, validation_data=[X_val, y_val])
    y_pred = clf.predict_proba(X)
    assert len(clf.coef_) == X.shape[1]
    assert all(clf.coef_ >= 0)



def test_tabimiso_build_args():
    clf = TabiMISO(orders=[3 for _ in range(NCOLS)], epochs=2,
                   build_args={'cont_params': dict(kernel_constraint=NonNeg(),
                                                   kernel_initializer='zeros',
                                                   kernel_regularizer=l1(0.01))})
    clf.fit(X, y)
    y_pred = clf.predict_proba(X)
    assert len(clf.coef_) == X.shape[1]
    assert all(clf.coef_ >= 0)


def test_tabimiso_polys():
    orders = [3 for _ in range(NCOLS)]
    clf = TabiMISO(orders=orders, epochs=2)
    clf.fit(X, y)
    for order, poly in zip(orders, clf.polys):
        assert len(poly) == (order + 1)

    orders = [3, 1, 3, 1, 3, 1, 3, 1, 3, 1]
    clf = TabiMISO(orders=orders, epochs=2)
    clf.fit(X, y)
    for order, poly in zip(orders, clf.polys):
        if order > 1:
            assert len(poly) == (order + 1)
        else:
            assert len(poly) == 2

    orders = [3, 1, 1, 1, 3, 3, 3, 1, 3, 1]
    clf = TabiMISO(orders=orders, epochs=2)
    clf.fit(X, y)
    for order, poly in zip(orders, clf.polys):
        if order > 1:
            assert len(poly) == (order + 1)
        else:
            assert len(poly) == 2


def test_tabimiso_order_1():
    clf = TabiMISO(orders=[3 for _ in range(NCOLS - 2)] + [1, 1], epochs=2)
    clf.fit(X, y)
    y_pred = clf.predict_proba(X)
    assert len(clf.coef_) == X.shape[1]


def test_tabimiso_funs():
    orders = [1, ] + [3 for _ in range(NCOLS - 3)] + [1, 1]
    clf = TabiMISO(orders=orders, epochs=2, optimizer='adam')
    clf.fit(X, y)

    funs = clf.risk_funs
    for n, x in enumerate(X.T):
        outimg = funs[n](x)
        assert len(outimg) == len(x)


def build_mode1(input_shape):
    video_input = Input(input_shape, name='video_input')
    # Video
    x = Conv3D(8, (3, 3, 3), padding='same')(video_input)
    x = MaxPooling3D((2, 2, 2))(x)
    x = BatchNormalization()(x)
    x = Conv3D(8, (3, 3, 3), padding='same')(x)
    x = MaxPooling3D((2, 2, 2))(x)
    x = BatchNormalization()(x)
    x = Conv3D(8, (3, 3, 3), padding='same')(x)
    x = MaxPooling3D((2, 2, 2))(x)
    x = BatchNormalization()(x)
    video_out = Flatten()(x)
    x = Dense(10, activation='relu')(video_out)
    output = Dense(1, activation='sigmoid', name='video_output')(x)
    model = Model(inputs=video_input, outputs=output)
    return model


def test_imiso_one_modal():
    mode1 = np.random.random((NSAMPLES, NCOLS, NCOLS, NCOLS, 1))
    clf = TabiMISO(orders=[3 for _ in range(NCOLS)],
                   build_modals=[build_mode1, ],
                   epochs=2)
    clf.fit([mode1, X], y)
    y_pred = clf.predict_proba([mode1, X])
    assert len(clf.coef_) == (X.shape[1] + 1)
    assert all(clf.coef_ >= 0)

def test_imiso_generator():

    orders = [3 for _ in range(NCOLS)]
    mode1 = np.random.random((NSAMPLES, NCOLS, NCOLS, NCOLS, 1))

    os.makedirs('deleteme', exist_ok=True)
    for i in range(NSAMPLES):
        np.save(f'deleteme/{i}.npy', mode1[i])

    train = list(range(NSAMPLES))
    val = list(range(NSAMPLES))

    yp = pd.Series(y.ravel())
    # Test generator
    train_generator = INNGenerator(
        orders      = orders,
        data_path   = 'deleteme', 
        y_set       = yp.loc[train], 
        batch_size  = 10,
        aux         = pd.DataFrame(X))
    val_generator = INNGenerator(
        orders      = orders,
        data_path   ='deleteme', 
        y_set       =yp.loc[val], 
        batch_size  = 10,
        aux         = pd.DataFrame(X))

    clf = TabiMISO(orders       = orders,
                   build_modals = [build_mode1, ],
                   interactions = True,
                   epochs       = 2)
    clf.fit(train_generator, validation_data=val_generator)
    y_pred = clf.predict(train_generator)

    clf = TabiMISO(orders       = orders,
                   build_modals = [build_mode1, ],
                   interactions = False,
                   epochs       = 2)
    clf.fit(train_generator, validation_data=val_generator)
    y_pred = clf.predict(train_generator)


    rmtree('deleteme')

def test_imiso_one_modal_cat():
    mode1 = np.random.random((NSAMPLES, NCOLS, NCOLS, NCOLS, 1))
    orders = [3, 3, 1, 1, 3, 3, 1, 1, 1, 3]
    clf = TabiMISO(orders=orders,
                   build_modals=[build_mode1, ],
                   epochs=20)
    clf.fit([mode1, X], y)
    y_pred = clf.predict_proba([mode1, X])
    assert len(clf.coef_) == (X.shape[1] + 1)
    assert all([c >= 0 for c, o in zip(clf.coef_, [3, ] + orders) if o == 3])

def test_save_load_INN():
    orders = [3, 1, 3, 1, 3, 1, 3, 1, 3, 1]
    clf = TabiMISO(orders=orders, epochs=2)
    clf.fit(X, y)
    ws0 = clf.saveINN('deleteme')

    ws1 = clf.loadINN('deleteme')

    assert ws0.keys() == ws1.keys()

def test_pretrain_INN():
    # pre-train
    orders = [3, 1, 3, 1, 3, 1, 3, 1, 3, 1]
    clf = TabiMISO(orders=orders, epochs=2)
    clf.fit(X, y)
    clf.saveINN('deleteme')

    mode1 = np.random.random((NSAMPLES, NCOLS, NCOLS, NCOLS, 1))
    clf = TabiMISO(orders=orders,
                   build_modals=[build_mode1, ],
                   pretrained_ws='deleteme',
                   epochs=2)
    clf.fit([mode1, X], y)

def test_pretrain_INN_w_interaction():
    # pre-train
    orders = [3, 1, 3, 1, 3, 1, 3, 1, 3, 1]
    clf = TabiMISO(orders=orders, epochs=2, interactions=True)
    clf.fit(X, y)
    clf.saveINN('deleteme')

    mode1 = np.random.random((NSAMPLES, NCOLS, NCOLS, NCOLS, 1))
    clf = TabiMISO(orders=orders,
                   build_modals=[build_mode1, ],
                   pretrained_ws='deleteme',
                   interactions=True,
                   epochs=2)
    clf.fit([mode1, X], y)

def teardown_module(module):

    os.remove('deleteme.pkl')
