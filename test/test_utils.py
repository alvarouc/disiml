import pytest
import sys
import os
import numpy as np
sys.path.insert(0, os.path.abspath(
    os.path.join(os.path.dirname(__file__), '../')))
from disiml import normalize_video

X = np.random.random((20, 10, 10, 1))


def test_normalize_video():
    Xn = normalize_video(video=X, downsample=2, timesteps=5)
    assert Xn.shape == (5, 5, 5, 1)

    Xn = normalize_video(video=X.squeeze(), downsample=2, timesteps=30)
    assert Xn.shape == (30, 5, 5, 1)
