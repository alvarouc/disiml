import pytest
import numpy as np
from sklearn.metrics import roc_auc_score
import pandas as pd
import sys
import os
from shutil import rmtree
sys.path.insert(0, os.path.abspath(
    os.path.join(os.path.dirname(__file__), '../')))  # noqa: E402
from disiml import VideoSISO, VideoMISO, FileBroker, FileBrokers  # noqa: E402

NFRAMES = 10
NCOLS = 20
NROWS = 20
NSAMPLES = 50

X = np.ones((NSAMPLES, NFRAMES, NCOLS, NROWS, 1))
X1 = np.ones((NSAMPLES, NFRAMES, NCOLS, NROWS, 1))
X_val = np.ones((NSAMPLES//10, NFRAMES, NCOLS, NROWS, 1))

y = np.random.uniform(low=0, high=1, size=(NSAMPLES, 1)) > .5
X[y.ravel()]+=50
X1[y.ravel()]+=20

y_val = np.random.uniform(low=0, high=1, size=(NSAMPLES//10, 1)) > .5
X_val[y_val.ravel()]+=50

aux = pd.DataFrame(np.random.random((NSAMPLES, 10)))

def test_siso_fit_predict():

    clf = VideoSISO(epochs=10, patience=5)
    clf.fit(X, y,
            weight_classes=True,
            sample_weight=np.ones((NSAMPLES, )))
    clf.fit(X, y,
            weight_classes=True,
            sample_weight=np.ones((NSAMPLES, )),
            validation_data=[X_val, y_val])

    y_pred = clf.predict(X).ravel()
    y_pred2 = clf.predict_proba(X).ravel()

    assert np.allclose(y_pred, y_pred2, atol=1e-4)


def test_siso_broker():

    os.makedirs('deleteme', exist_ok=True)
    for i in range(NSAMPLES):
        np.save(f'deleteme/{i}.npy', X[i])

    broker = FileBroker('deleteme')
    train = list(range(NSAMPLES//2))
    val = list(range(NSAMPLES//2,NSAMPLES))

    clf = VideoSISO(epochs=10, patience=5)
    clf.fit_batches(broker, train, val, pd.Series(y.ravel()))

    y_pred3 = clf.predict_batches(broker, range(NSAMPLES))

    rmtree('deleteme')


def test_max_activation():
    clf = VideoSISO(epochs=10, patience=2)
    clf.fit(X, y)
    names = [layer['name'] for layer in clf.model.get_config()['layers']]
    imgs = clf.max_activation(names[1])
    assert isinstance(imgs, list)


def test_miso_fit_predict():

    clf = VideoMISO(epochs=10, patience=2)
    clf.fit([X, X1], y)
    clf.fit([X, X1], y, validation_data=[[X_val, X_val / 16], y_val])

    y_pred = clf.predict([X, X1]).ravel()
    y_pred2 = clf.predict_proba([X, X1]).ravel()
  
def test_fit_batches():

    folders = ['deleteme1', 'deleteme2']
    for folder in folders:
        os.makedirs(folder, exist_ok=True)
        for i in range(NSAMPLES):
            np.save(f'{folder}/{i}.npy', X[i])

    broker = FileBrokers(folders)
    train = list(range(NSAMPLES//2))
    val = list(range(NSAMPLES//2,NSAMPLES))

    clf = VideoMISO(epochs=10, patience=5, keep_best=False)
    clf.fit_batches(broker, train, val, pd.Series(y.ravel()))
    y_pred3 = clf.predict_batches(broker, train)

    [rmtree(folder) for folder in folders]


def test_video_miso_w_aux():

    clf = VideoMISO(epochs=2)
    clf.fit([X, aux], y)
    y_pred = clf.predict_proba([X, aux])


def test_build_fun_as_argument():
    from tensorflow.python.keras.models import Model
    from tensorflow.python.keras.layers.wrappers import TimeDistributed
    from tensorflow.python.keras.layers import (Input, BatchNormalization,
                                                Dense, CuDNNLSTM, LSTM,
                                                Conv2D, Conv3D,
                                                ConvLSTM2D,
                                                MaxPooling2D, MaxPooling3D,
                                                Flatten, concatenate)
    from tensorflow.python.keras.backend import floatx

    def build_model(input_shape, loss, optimizer, metrics=None, num_class=1, *args, **kwargs):
        video_input = Input(shape=(NFRAMES, NCOLS, NROWS, 1),
                            dtype=floatx(),
                            name='video_input')

        # Video
        x = Conv3D(8, (3, 3, 3), padding='same')(video_input)
        x = MaxPooling3D((2, 2, 2))(x)
        x = BatchNormalization()(x)
        x = Conv3D(8, (3, 3, 3), padding='same')(x)
        x = MaxPooling3D((2, 2, 2))(x)
        x = BatchNormalization()(x)

        video_out = Flatten()(x)
        # EHR
        aux_input = Input(shape=(10,), name='aux_input')
        aux_out = Dense(10, activation='relu')(aux_input)
        # Video + EHR
        x = concatenate([video_out, aux_out])
        x = Dense(50, activation='relu')(x)
        output = Dense(1, activation='sigmoid')(x)

        inputs = [video_input, aux_input]
        model = Model(inputs=inputs, outputs=output)
        model.compile(loss=loss,optimizer=optimizer, metrics=metrics)

        return model

    clf = VideoMISO(epochs=2, build_fun=build_model)
    clf.fit([X, aux], y)
    y_pred = clf.predict_proba([X, aux])


def test_validation():

    aux_val = pd.DataFrame(np.random.random((X_val.shape[0], 10)))

    clf = VideoMISO(epochs=2)
    clf.fit([X, aux], y, validation_data=[[X_val, aux_val], y_val])
    y_pred = clf.predict_proba([X, aux])

    clf = VideoSISO(epochs=2)
    clf.fit(X, y, validation_data=[X_val, y_val])
    y_pred = clf.predict_proba(X)
